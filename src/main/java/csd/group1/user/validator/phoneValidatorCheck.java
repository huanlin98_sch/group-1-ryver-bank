package csd.group1.user.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class phoneValidatorCheck implements ConstraintValidator<phoneValidator, String> {
    String phone;

    @Override
    public void initialize(phoneValidator constraint) {
        this.phone = constraint.value();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null) {
            return true;
        }
        return validatePhone(s);
    }

    public boolean validatePhone(String phone) {
        if (phone.length() != 8) {
            return false;
        }

        if (!phone.startsWith("8") && !phone.startsWith("9")) {
            return false;
        }

        return true;
    }

}
