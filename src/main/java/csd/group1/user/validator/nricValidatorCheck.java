package csd.group1.user.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class nricValidatorCheck implements ConstraintValidator<nricValidator, String> {
    private String nric;

    @Override
    public void initialize(nricValidator constraint) {
        this.nric = constraint.value();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null) {
            return true;
        }
        return validateNRIC(s);
    }

    public static boolean validateNRIC(String nric) {
        if (nric.length() != 9)
            return false;

        nric = nric.toUpperCase();

        String[] strArray = new String[9];
        for (int i = 0; i < 9; i++) {
            strArray[i] = nric.charAt(i) + "";
        }

        strArray[1] = Integer.toString(Integer.parseInt(strArray[1]) * 2);
        strArray[2] = Integer.toString(Integer.parseInt(strArray[2], 10) * 7);
        strArray[3] = Integer.toString(Integer.parseInt(strArray[3], 10) * 6);
        strArray[4] = Integer.toString(Integer.parseInt(strArray[4], 10) * 5);
        strArray[5] = Integer.toString(Integer.parseInt(strArray[5], 10) * 4);
        strArray[6] = Integer.toString(Integer.parseInt(strArray[6], 10) * 3);
        strArray[7] = Integer.toString(Integer.parseInt(strArray[7], 10) * 2);

        int weight = 0;
        for (int i = 1; i < 8; i++) {
            weight += Integer.parseInt(strArray[i]);
        }

        int offset = (strArray[0].equals("T") || strArray[0].equals("G")) ? 4 : 0;
        int temp = (offset + weight) % 11;

        String[] st = { "J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A" };
        String[] fg = { "X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K" };

        String theAlpha = "";
        if (strArray[0].equals("S") || strArray[0].equals("T")) {
            theAlpha = st[temp];
        } else if (strArray[0].equals("F") || strArray[0].equals("G")) {
            theAlpha = fg[temp];
        }

        return (strArray[8].equals(theAlpha));
    }
}
