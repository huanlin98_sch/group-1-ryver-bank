package csd.group1.user;

import java.util.List;

public interface UsersService {

    User addUser(User user);

    User updateUser(int id, User user);

    User deactivate(int id);

    User customerDetailsUpdate(int id, User user);

    User viewCustomerInfo(int id);

    List<User> listUsers();
}
