package csd.group1.user;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UsersController {
    private BCryptPasswordEncoder encoder;
    private UsersService usersService;
    private UserRepository users;

    public UsersController(BCryptPasswordEncoder encoder, UsersService usersService, UserRepository users) {
        this.encoder = encoder;
        this.usersService = usersService;
        this.users = users;
    }

//    this end point is only for managers to view all customers
    @GetMapping("/customers")
    public List<User> getUsers() {
        return usersService.listUsers();
    }

//    this endpoint is for customers to view their own details
    @GetMapping("/customers/{id}")
    public User viewCustomerInfo(@PathVariable int id) {
        return usersService.viewCustomerInfo(id);
    }

//    this end point is for managers to create new customers
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/customers")
    public User addUser(@Valid @RequestBody User user) {
        user = encodeUserPassword(user);
        return usersService.addUser(user);
    }

//    this end point is for customers/managers to update the user details
    @PutMapping("/customers/{id}")
    public User updateUser(@PathVariable int id, @Valid @RequestBody User newUserInfo,
            Authentication authentication) {
//        this method checks whether such a user with the specified ID exists
        checkValidUser(id);
//        this method is to encode the password for updating
        newUserInfo = encodeUserPassword(newUserInfo);
        User user;
//        this is to check the authority of the logged in user before redirecting to the respective service methods
        if (checkUserAuthorisation(authentication)) {
            user = usersService.customerDetailsUpdate(id, newUserInfo);
        } else {
            user = usersService.updateUser(id, newUserInfo);
        }
        return user;
    }

    @PutMapping("/customers/deactivate/{id}")
    public User deactivateUser(@PathVariable int id) {
        checkValidUser(id);
        User user = usersService.deactivate(id);
        return user;
    }

    private void checkValidUser(int id) {
        if (users.findById(id).orElse(null) == null) {
            throw new InvalidUserDetailsException(id);
        }
    }

    private User encodeUserPassword(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        return user;
    }

    private boolean checkUserAuthorisation (Authentication authentication) {
        return "ROLE_USER".equals(authentication.getAuthorities().iterator().next().toString());
    }


}
