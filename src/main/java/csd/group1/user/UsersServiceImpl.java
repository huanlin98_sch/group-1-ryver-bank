package csd.group1.user;

import csd.group1.exception.InactiveUserException;
import csd.group1.portfolio.Portfolio;
import csd.group1.portfolio.PortfolioRepository;
import lombok.SneakyThrows;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService, UserDetailsService {
    private UserRepository users;
    private PortfolioRepository portfolioRepository;

    public UsersServiceImpl(UserRepository userRepository, PortfolioRepository portfolioRepository) {
        this.users = userRepository;
        this.portfolioRepository = portfolioRepository;
    }

    @Override
    public List<User> listUsers() {
        return users.findAll();
    }

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (users.findByUsername(username).get().getActive() == false) {
            throw new InactiveUserException(username);
        }
        return users.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User '" + username + "' not found"));
    }

    @Override
    public User viewCustomerInfo(int id) {
        return users.findById(id).orElse(null);
    }

    @Override
    public User addUser(User user) {
        String username = user.getUsername();
        if (users.findByUsername(username).orElse(null) != null) {
            throw new UsernameTakenException(username);
        }
        User savedUser = users.save(user);
        if (user.getAuthorities().iterator().next().toString().equals("ROLE_USER")) {
            portfolioRepository.save(new Portfolio(savedUser.getId()));
        }
        return savedUser;
    }

    @Override
    public User updateUser(int id, User newUserInfo) {
        return users.findById(id).map(user -> {
            user.setPassword(newUserInfo.getPassword());
            user.setAddress(newUserInfo.getAddress());
            user.setFullName(newUserInfo.getFullName());
            user.setNric(newUserInfo.getNric());
            user.setPhone(newUserInfo.getPhone());
            user.setActive(newUserInfo.getActive());
            user.setAuthorities(newUserInfo.getAuthorities().iterator().next().toString());
            return users.save(user);
        }).orElse(null);
    }

    @Override
    public User customerDetailsUpdate(int id, User newUserInfo) {
        return users.findById(id).map(user -> {
            user.setAddress(newUserInfo.getAddress());
            user.setPassword(newUserInfo.getPassword());
            user.setPhone(newUserInfo.getPhone());
            return users.save(user);
        }).orElse(null);
    }

    @Override
    public User deactivate(int id) {
        return users.findById(id).map(user -> {
            user.setActive(false);
            users.save(user);
            return user;
        }).orElse(null);
    }

}
