package csd.group1.portfolio;


public interface PortfolioService {
    Portfolio getPortfolio(int portfolioId);
    double getTotalGainLossWithoutUnrealizedGainLoss(Portfolio portfolio);
    double getUnrealizedGainLoss(Portfolio portfolio);
    Portfolio addPortfolio(Portfolio portfolio);
    Portfolio addInsights(int portfolioId, Insight insight);
}
