package csd.group1.portfolio;

import javax.validation.Valid;

import csd.group1.user.User;
import csd.group1.user.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PortfolioController {
    private PortfolioService portfolioService;
    private UserRepository users;

    public PortfolioController(PortfolioService portfolioService, UserRepository users) {
        this.portfolioService = portfolioService;
        this.users = users;
    }

    @GetMapping("/portfolio")
    public Portfolio getPortfolio(Authentication authentication) {

        User customer = users.findByUsername(authentication.getName()).orElse(null);

        // get portfolioId = customerId
        int portfolioId = customer.getId();

        // managers and analysts do not have portfolios
        if ("ROLE_MANAGER".equals(authentication.getAuthorities().iterator().next().toString()) 
            || "ROLE_ANALYST".equals(authentication.getAuthorities().iterator().next().toString())) {
                throw new PortfolioNotFoundException(portfolioId);
            }

        Portfolio portfolio = portfolioService.getPortfolio(portfolioId);

        return portfolio;
    }

    @PutMapping("/portfolio/{portfolioId}") 
    public Portfolio updatePortfolio(@PathVariable int portfolioId, @Valid @RequestBody Insight insight, Authentication authentication) {
        insight.setAnalystName(authentication.getName());
        return portfolioService.addInsights(portfolioId, insight);
    }
}
