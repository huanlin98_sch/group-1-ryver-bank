package csd.group1.portfolio;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidCustomerException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public InvalidCustomerException(int id) {
    super("Invalid customer ID");
  }
}
