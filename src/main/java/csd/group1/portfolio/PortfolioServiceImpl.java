package csd.group1.portfolio;

import java.util.*;
import org.springframework.stereotype.Service;

import csd.group1.user.*;
import csd.group1.stock.*;
import csd.group1.asset.*;

@Service
public class PortfolioServiceImpl implements PortfolioService {
    private PortfolioRepository portfolios;
    private StockServiceImpl stockService;
    private AssetRepository assets;

    public PortfolioServiceImpl(PortfolioRepository portfolios, StockServiceImpl stockService, AssetRepository assets) {
        this.portfolios = portfolios;
        this.stockService = stockService;
        this.assets = assets;
    }

    @Override
    public Portfolio getPortfolio(int portfolioId) {
        Portfolio portfolio = portfolios.findById(portfolioId)
            .orElseThrow(() -> new UserNotFoundException(portfolioId));

        // get "total gain loss" for all assets excluding the current ones the user own
        double getTotalGLWithoutUnrealizedGL = getTotalGainLossWithoutUnrealizedGainLoss(portfolio);

        // update final "unrealized gain loss" in portfolio
        double unrealizedGainLoss = getUnrealizedGainLoss(portfolio);
        portfolio.setUnrealizedGainLoss(unrealizedGainLoss);

        // update final "total gain loss" in portfolio by adding "unrealized gain loss" to previous total
        portfolio.setTotalGainLoss(getTotalGLWithoutUnrealizedGL + unrealizedGainLoss);

        portfolios.save(portfolio);
        return portfolio;
    }

    @Override 
    // called from getPorfolio
    public double getTotalGainLossWithoutUnrealizedGainLoss(Portfolio portfolio) {
        double totalGainLoss = portfolio.getTotalGainLoss();
        double unrealizedGainLoss = portfolio.getUnrealizedGainLoss();
        totalGainLoss -= unrealizedGainLoss;
        return totalGainLoss;
    }

    @Override
    // called from getPorfolio
    public double getUnrealizedGainLoss(Portfolio portfolio) {
        // get all assets to loop through later
        List<Asset> allAssets = assets.findByPortfolioId(portfolio.getId());

        // variables needed in the for loop
        Asset individualAsset = null;
        String individualAssetSymbol = null;
        double currentPrice = 0.0;
        double unrealizedGainLoss = 0.0;

        // set the fields for each asset to the updated one in stock repo
        for (int i = 0; i < allAssets.size(); i++) {
            // set current price
            individualAsset = allAssets.get(i);
            individualAssetSymbol = individualAsset.getCode();
            
            currentPrice = stockService.getStock(individualAssetSymbol).getLastPrice();
            individualAsset.setCurrentPrice(currentPrice);

            // set value
            double newValue = currentPrice * individualAsset.getQuantity();
            individualAsset.setValue(newValue);

            // set gain_loss
            double gainLoss = newValue - individualAsset.getAvgPrice() * individualAsset.getQuantity();
            individualAsset.setGainLoss(gainLoss);

            // update "unrealized gain loss"
            unrealizedGainLoss += gainLoss;
        }

        return unrealizedGainLoss;
    }

    @Override
    public Portfolio addInsights(int portfolioId, Insight insight) {
        Portfolio portfolio = portfolios.findById(portfolioId).orElse(null);
        if (portfolio == null)
            throw new InvalidCustomerException(portfolioId);

        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Singapore"));
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Singapore");

        Calendar currTime = Calendar.getInstance(timeZone);
        insight.setDate(currTime);
        portfolio.getInsights().add(insight);
        return portfolios.save(portfolio);
    }

    // only for creating a portfolio when a user is created
    @Override
    public Portfolio addPortfolio(Portfolio portfolio) {
        return portfolios.save(portfolio);
    }
}
