package csd.group1.portfolio;

import java.util.*;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import csd.group1.asset.*;

import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Portfolio {

    // every customer has one portfolio
    // @JsonIgnore  
    // private @Id @GeneratedValue (strategy = GenerationType.IDENTITY) int portfolioNumber;

    @JsonProperty("customer_id") 
    private @Id int id;

    // one portfolio has multiple assets
    @JsonProperty("assets") 
    @OneToMany(mappedBy = "portfolio", cascade = CascadeType.ALL)
    private List<Asset> assets;

    @JsonProperty("unrealized_gain_loss")
    private double unrealizedGainLoss = 0.0;

    @JsonProperty("total_gain_loss")
    private double totalGainLoss = 0.0;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonProperty("insights")
    private List<Insight> insights;

    public Portfolio(int customerId) {
        this.id = customerId;
    }
}