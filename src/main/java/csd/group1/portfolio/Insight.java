package csd.group1.portfolio;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Insight {
  private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) int id;
  private String analystName;
  @NotNull
  private String analysis;
  private Calendar date;
}
