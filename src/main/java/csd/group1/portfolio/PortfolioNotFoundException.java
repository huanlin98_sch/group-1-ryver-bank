package csd.group1.portfolio;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND) // 404 Error
public class PortfolioNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public PortfolioNotFoundException(int id) {
        super("Managers and analysts do not have portfolios.");
    }
}
