package csd.group1.configuration;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.*;
import io.swagger.v3.oas.annotations.servers.Server;


@OpenAPIDefinition(
    info = @Info(
        title = "RyverBank API",
        description = "This is an API for RyverBank Application",
        contact = @Contact(
            name = "Group 1 Ryver Bank - Collaborative Software Development", 
            url = "Your Website", 
            email = "youremail@smu.edu.sg"),
        license = @License(
            name = "Apache License 2.0", 
            url = "NA")),
        servers = @Server(url = "http://localhost:8080")
)
public class OpenAPIConfig {
    
}