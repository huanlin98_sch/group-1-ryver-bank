package csd.group1;

import csd.group1.user.*;
import csd.group1.account.AccountRepository;
import csd.group1.content.*;
import csd.group1.stock.Stock;
import csd.group1.stock.StockRepository;
import csd.group1.txn.TxnRepository;
import csd.group1.trade.Trade;
import csd.group1.trade.TradeRepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import yahoofinance.YahooFinance;

import java.io.IOException;

@SpringBootApplication
public class BankApplication {

    private static ConfigurableApplicationContext ctx;

    public static void main(String[] args) {
        ctx = SpringApplication.run(BankApplication.class, args);
        init();
    }

    public static void init() {
        UserRepository userRepo = ctx.getBean(UserRepository.class);
        ContentRepository contentRepo = ctx.getBean(ContentRepository.class);
        AccountRepository accountRepo = ctx.getBean(AccountRepository.class);
        TxnRepository txnRepo = ctx.getBean(TxnRepository.class);

        // DELETE THIS LATER
        StockRepository stockRepo = ctx.getBean(StockRepository.class);
        TradeRepository tradeRepo = ctx.getBean(TradeRepository.class);
        BCryptPasswordEncoder encoder = ctx.getBean(BCryptPasswordEncoder.class);
        String m_USERNAME = "manager_1";
        String m_PASSWORD = "01_manager_01";
        userRepo.save(createEncodedUser(m_USERNAME, m_PASSWORD, "ROLE_MANAGER", encoder));

        // analyst account, role: ROLE_ANALYST
        // should be added to your api before the test starts
        String a1_USERNAME = "analyst_1";
        String a1_PASSWORD = "01_analyst_01";
        String a2_USERNAME = "analyst_2";
        String a2_PASSWORD = "02_analyst_02";
        userRepo.save(createEncodedUser(a1_USERNAME, a1_PASSWORD, "ROLE_ANALYST", encoder));
        userRepo.save(createEncodedUser(a2_USERNAME, a2_PASSWORD, "ROLE_ANALYST", encoder));

        stockPuller(stockRepo, tradeRepo);
    }

    public static User createEncodedUser(String username, String password, String authority,
            BCryptPasswordEncoder encoder) {
        return new User(username, encoder.encode(password), authority);
    }

    public static void stockPuller(StockRepository stockRepository, TradeRepository tradeRepository) {
        YahooFinance yahooFinance = new YahooFinance();
        String[] stiStocks = new String[] {
            "A17U", "C61U", "C31", "C38U", "C09", "C52", "D01", "D05", "G13", "H78",
            "C07", "J36", "J37", "BN4", "N2IU", "ME8U", "M44U", "O39", "S58", "U96",
            "S68", "C6L", "Z74", "S63", "Y92", "U11", "U14", "V03", "F34", "BS6"
        };
        for (String symbol : stiStocks) {
            try {
                yahoofinance.Stock stock = yahooFinance.get(symbol + ".SI");
                double lastPrice = stock.getQuote().getPrice().doubleValue();
                double bidPrice = stock.getQuote().getBid().doubleValue() - 0.10;
                double askPrice = stock.getQuote().getAsk().doubleValue() + 0.10;
                stockRepository.save(new Stock(symbol, lastPrice, 20000, bidPrice, 20000, askPrice));
                tradeRepository.save(new Trade(symbol, "buy", 20000, 0, 0, bidPrice, 0.0, "open"));
                tradeRepository.save(new Trade(symbol, "sell", 20000, 0, 0, 0.0, askPrice, "open"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
