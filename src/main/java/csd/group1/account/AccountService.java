package csd.group1.account;

import java.util.List;

public interface AccountService {

    List<Account> listAccounts();

    List<Account> getAllAccountsByUserId(int userId);

    Account getAccount(int id);

    Account addAccount(Account account);

}