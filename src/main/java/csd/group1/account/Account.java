package csd.group1.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Account {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) int id;

    @NotNull
    private int customerId;

    @NotNull
    private double balance;

    // @NotNull
    private double availableBalance;

    @JsonProperty("customer_id")
    public int getCustomerId() {
        return customerId;
    }

    @JsonProperty("available_balance")
    public double getAvailableBalance() {
        return availableBalance;
    }

    public Account(int customer_id, double balance, double availableBalance) {
        this.customerId = customer_id;
        this.balance = balance;
        this.availableBalance = availableBalance;
    }

    public void updateBothBalances(double amount) {
        this.setBalance(this.balance + amount);
        this.setAvailableBalance(this.availableBalance + amount);
    }
}