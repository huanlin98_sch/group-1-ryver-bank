package csd.group1.account;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import csd.group1.user.UserRepository;
import csd.group1.user.User;

@RestController
public class AccountController {
    private AccountRepository accounts;
    private UserRepository users;

    public AccountController(AccountRepository accounts, UserRepository users) {
        this.accounts = accounts;
        this.users = users;
    }

    @GetMapping("/accounts")
    public List<Account> getAllOwnAccounts(Authentication authentication) {
        User customer = getCustomer(authentication);
        return accounts.findByCustomerId(customer.getId());
    }

    // GET: Throws either 403 forbidden or 404 not_found only.
    @GetMapping("/accounts/{accountId}")
    public Account getAccount(@PathVariable(value = "accountId") int accountId, Authentication authentication) {
        User customer = getCustomer(authentication);
        // Check if account with accountId exists (404 not_found if does not exist)
        Account account = accounts.findById(accountId).orElse(null);
        if (account == null) {
            throw new AccountNotFoundException(accountId);
        }

        // Check if customer owns the account stated in accountId (403 forbidden if
        // account does not belong to user)
        if (account.getCustomerId() != customer.getId()) {
            throw new AccountForbiddenException(accountId);
        }
        return account;
    }

    // POST: Throws either 400 bad_request or 403 forbidden only.
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/accounts")
    public Account addAccount(@Valid @RequestBody Account account) {
        User customer = users.findById(account.getCustomerId()).orElse(null);
        // Check if user with entered id exists (400 bad_request if does not exist)
        if (customer == null) {
            throw new InvalidAccountDetailsException();
        }
        // Customer cannot be manager/analyst
        if (!customer.getAuthorities().iterator().next().toString().equals("ROLE_USER")) {
            throw new InvalidAccountDetailsException();
        }
        account.setAvailableBalance(account.getBalance());
        return accounts.save(account);
    }

    // If user is manager/analyst, no account --> 403.
    public User getCustomer(Authentication authentication) {
        if (authentication.getAuthorities().iterator().next().toString().equals("ROLE_MANAGER")
                || authentication.getAuthorities().iterator().next().toString().equals("ROLE_ANALYST")) {
            throw new NoAccountException();
        }
        User customer = users.findByUsername(authentication.getName()).orElse(null);
        return customer;
    }
}