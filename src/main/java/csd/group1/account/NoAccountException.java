package csd.group1.account;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class NoAccountException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NoAccountException() {
        super("Managers and analysts have no accounts.");
    }
}
