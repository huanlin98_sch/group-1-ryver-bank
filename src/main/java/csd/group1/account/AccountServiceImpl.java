package csd.group1.account;

import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {
    private AccountRepository accounts;

    public AccountServiceImpl(AccountRepository accounts) {
        this.accounts = accounts;
    }

    @Override
    public List<Account> listAccounts() {
        return accounts.findAll();
    }

    @Override
    public List<Account> getAllAccountsByUserId(int userId) {
        return accounts.findByCustomerId(userId);
    }

    @Override
    public Account getAccount(int id) {
        return accounts.findById(id).orElse(null);
    }

    @Override
    public Account addAccount(Account account) {
        return accounts.save(account);
    }

}