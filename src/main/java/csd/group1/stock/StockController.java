package csd.group1.stock;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StockController {
    private StockService stockService;

    public StockController(StockService ss) {
        this.stockService = ss;
    }

    @GetMapping("/stocks")
    public List<Stock> getStocks() {
        return stockService.listStocks();
    }

    @GetMapping("/stocks/{symbol}")
    public Stock getStock(@PathVariable String symbol) {
        Stock stock = stockService.getStock(symbol);
        if (stock == null) throw new StockNotFoundException(symbol);

        return stock;
    }

    // if price changes due to purchase or sale of this stock
    // won't be tested
    // won't be checking authentication here but at the trade package
    @PutMapping("/stocks/{symbol}")
    public Stock updateStock(@PathVariable String symbol, @RequestBody Stock newStockInfo) {
        Stock stock = stockService.getStock(symbol);
        if (stock == null) 
            throw new StockNotFoundException(symbol);
        stock = stockService.updateStock(symbol, newStockInfo);
        return stock;
    }

}
