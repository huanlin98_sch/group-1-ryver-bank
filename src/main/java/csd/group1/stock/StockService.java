package csd.group1.stock;

import java.util.List;

public interface StockService {
    List<Stock> listStocks();
    Stock getStock(String symbol);
    Stock updateStock(String symbol, Stock newStockInfo);
}
