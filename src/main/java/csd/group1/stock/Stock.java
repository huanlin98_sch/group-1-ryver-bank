package csd.group1.stock;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode

public class Stock {
    
    private @Id String symbol;

    @JsonProperty("last_price")
    private double lastPrice;

    @JsonProperty("bid_volume")
    private int bidVolume;

    private double bid;

    @JsonProperty("ask_volume")
    private int askVolume;

    private double ask;

    public Stock (String symbol, double lastPrice, int bidVolume, double bid, int askVolume, double ask) {
        this.symbol = symbol;
        this.lastPrice = lastPrice;
        this.bidVolume = bidVolume;
        this.bid = bid;
        this.askVolume = askVolume;
        this.ask = ask;
    }
}
