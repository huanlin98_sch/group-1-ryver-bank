package csd.group1.stock;

import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class StockServiceImpl implements StockService {
    private StockRepository stocks;

    public StockServiceImpl(StockRepository stocks) {
        this.stocks = stocks;
    }

    @Override
    public List<Stock> listStocks() {
        return stocks.findAll();
    }
    
    @Override 
    public Stock getStock(String symbol) {
        return stocks.findBySymbol(symbol)
            .orElseThrow(() -> new StockNotFoundException(symbol));
    }

    @Override
    public Stock updateStock(String symbol, Stock newStockInfo) {
        return stocks.findBySymbol(symbol).map(stock -> {
            stock.setLastPrice(newStockInfo.getLastPrice());
            stock.setBid(newStockInfo.getBid());
            stock.setAsk(newStockInfo.getAsk());
            return stocks.save(stock);
        }).orElse(null);
    }
}
