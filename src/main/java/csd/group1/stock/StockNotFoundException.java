package csd.group1.stock;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND) // 404 Error
public class StockNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public StockNotFoundException(String symbol) {
        super("Could not find stock " + symbol);
    }
}
