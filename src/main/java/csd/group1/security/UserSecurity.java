package csd.group1.security;

import csd.group1.user.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component("userSecurity")
public class UserSecurity {

    private UserRepository users;

    public UserSecurity(UserRepository users) {
        this.users = users;
    }

    public boolean correctUser(Authentication authentication, int id) {
        if ("ROLE_MANAGER".equals(authentication.getAuthorities().iterator().next().toString())) {
            return true;
        }
        return authentication.getName().equals(users.findById(id).orElse(null).getUsername());
    }
}
