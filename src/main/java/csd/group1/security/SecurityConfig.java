package csd.group1.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private UserDetailsService userDetailsService;

    public SecurityConfig(UserDetailsService userSvc) {
        this.userDetailsService = userSvc;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(encoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests().antMatchers(HttpMethod.GET, "/customers").hasRole("MANAGER")
                .antMatchers(HttpMethod.POST, "/customers").hasRole("MANAGER")
                .antMatchers(HttpMethod.GET, "/customers/{userId}")
                .access("@userSecurity.correctUser(authentication, #userId)")
                .antMatchers(HttpMethod.PUT, "/customers/{userId}")
                .access("@userSecurity.correctUser(authentication, #userId)")
                .antMatchers(HttpMethod.PUT, "customers/deactivate/*").hasRole("MANAGER")

                .antMatchers(HttpMethod.GET, "/contents", "/contents/*").authenticated()
                .antMatchers(HttpMethod.POST, "/contents").hasAnyRole("ANALYST", "MANAGER")
                .antMatchers(HttpMethod.PUT, "/contents/*").hasAnyRole("ANALYST", "MANAGER")
                .antMatchers(HttpMethod.DELETE, "/contents/*").hasAnyRole("ANALYST", "MANAGER")

                .antMatchers(HttpMethod.POST, "/accounts").hasRole("MANAGER")
                .antMatchers(HttpMethod.GET, "/accounts", "/accounts/*").authenticated()

                .antMatchers(HttpMethod.POST, "/trades").authenticated()
                .antMatchers(HttpMethod.GET, "/trades").hasAnyRole("ANALYST", "MANAGER")
                .antMatchers(HttpMethod.GET, "/trades/*").authenticated()
                .antMatchers(HttpMethod.PUT, "/trades/*").authenticated()

                .antMatchers(HttpMethod.GET, "/portfolio").authenticated()
                .antMatchers(HttpMethod.POST, "/portfolio").hasAnyRole("ANALYST", "MANAGER")
                .antMatchers(HttpMethod.PUT, "/portfolio/*").hasAnyRole("ANALYST", "MANAGER")

                // .antMatchers(HttpMethod.GET,
                // "accounts/{accountId}/transactions").authenticated()
                // .antMatchers(HttpMethod.POST,
                // "accounts/{accoundId}/transactions").hasRole("USER")

                .and().csrf().disable() // CSRF protection is needed only for browser based attacks
                .formLogin().disable().headers().disable().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests().antMatchers("/**").hasRole("USER").and().formLogin().and()
                // sample anonymous customization
                .anonymous().disable();
    }


    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/reset");
        web.ignoring().antMatchers("/v3/api-docs/**", "/swagger-ui.html", "/swagger-ui/**");

    }

    @Bean
    public BCryptPasswordEncoder encoder() {
        // auto-generate a random salt internally
        return new BCryptPasswordEncoder();
    }
}
