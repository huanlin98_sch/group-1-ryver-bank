package csd.group1;

import csd.group1.account.AccountRepository;
import csd.group1.asset.AssetRepository;
import csd.group1.content.ContentRepository;
import csd.group1.portfolio.PortfolioRepository;
import csd.group1.stock.StockRepository;
import csd.group1.trade.Trade;
import csd.group1.trade.TradeRepository;
import csd.group1.txn.Txn;
import csd.group1.txn.TxnRepository;
import csd.group1.user.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ResponseStatus(HttpStatus.OK)
@RestController
public class RestartController {
  UserRepository userRepo;
  ContentRepository contentRepo;
  AccountRepository accountRepo;
  TxnRepository txnRepo;
  StockRepository stockRepo;
  TradeRepository tradeRepo;
  PortfolioRepository portfolioRepository;
  AssetRepository assetRepository;

  public RestartController(UserRepository userRepository, ContentRepository contentRepo, AccountRepository accountRepository,
                           TxnRepository txnRepository, StockRepository stockRepository, TradeRepository tradeRepository,
                           PortfolioRepository portfolioRepository, AssetRepository assetRepository) {
    this.userRepo = userRepository;
    this.contentRepo = contentRepo;
    this.accountRepo = accountRepository;
    this.txnRepo = txnRepository;
    this.stockRepo = stockRepository;
    this.tradeRepo = tradeRepository;
    this.portfolioRepository = portfolioRepository;
    this.assetRepository = assetRepository;
  }
  @PostMapping("/reset")
  public void restart() {
    userRepo.deleteAll();
    contentRepo.deleteAll();
    accountRepo.deleteAll();
    txnRepo.deleteAll();
    stockRepo.deleteAll();
    tradeRepo.deleteAll();
    portfolioRepository.deleteAll();
    assetRepository.deleteAll();

    BankApplication.init();
  }
}
