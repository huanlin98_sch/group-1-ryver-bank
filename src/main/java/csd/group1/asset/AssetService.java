package csd.group1.asset;
import java.util.List;

public interface AssetService {
    List<Asset> getAllAssetByPortfolioId(int portfolioId);

    Asset addAsset(int portfolioId, Asset asset);

    Asset editingExistingAsset (int portfolioId, String code,
                                int changeInQuantity, double newPrice, String action);

    void deleteAsset(int portfolioId, String code);
}
