package csd.group1.asset;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;
import csd.group1.portfolio.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Asset {

    @JsonIgnore
    private @Id @GeneratedValue (strategy = GenerationType.IDENTITY) int assetId;

    @NotNull(message = "Stock code should not be null")
    private String code;

    private int quantity;

    @JsonProperty("avg_price")
    private double avgPrice;

    @JsonProperty("current_price")
    private double currentPrice;

    private double value;
    
    @JsonProperty("gain_loss")
    private double gainLoss;

    @ManyToOne
    @JoinColumn(name = "portfolio_id", nullable = false)
    @JsonIgnore
    private Portfolio portfolio;

    public Asset(String code, int quantity, double avgPrice) {
        this.code = code;
        this.quantity = quantity;
        this.avgPrice = avgPrice;
    }
}
