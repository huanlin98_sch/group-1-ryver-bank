package csd.group1.asset;

import java.util.List;

import org.springframework.stereotype.Service;

import csd.group1.portfolio.*;
import csd.group1.user.UserNotFoundException;

@Service
public class AssetServiceImpl implements AssetService {
    private AssetRepository assets;
    private PortfolioRepository portfolios;
    private PortfolioServiceImpl portfolioImpl;

    public AssetServiceImpl(AssetRepository assets, PortfolioRepository portfolios, PortfolioServiceImpl portfolioImpl) {
        this.assets = assets;
        this.portfolios = portfolios;
        this.portfolioImpl = portfolioImpl;
    }

    @Override
    public List<Asset> getAllAssetByPortfolioId(int portfolioId) {
        if (!portfolios.existsById(portfolioId)) {
            throw new PortfolioNotFoundException(portfolioId);
        }
        return assets.findByPortfolioId(portfolioId);
    }

    @Override
    public Asset addAsset(int portfolioId, Asset asset) {
        return portfolios.findById(portfolioId).map(portfolio ->{
            asset.setPortfolio(portfolio);
            assets.save(asset);
            // used the getPortfolio method to update unrealized and total gain losses
            portfolioImpl.getPortfolio(portfolioId);
            return asset;
        }).orElseThrow(() -> new UserNotFoundException(portfolioId));
    }

    @Override
    public Asset editingExistingAsset (int portfolioId, String code,
                                int changeInQuantity, double tradePrice, String action) {
        // for buy trade
        if (action == "buy") {
            return assets.findByCodeAndPortfolioId(code, portfolioId).map(asset -> {

                // update quantity and avg price
                updateAssetForEditingBuyTrade(asset, changeInQuantity, tradePrice);

                // use the getPortfolio method to update unrealized and total gain losses
                portfolioImpl.getPortfolio(portfolioId);
                return asset;

            }).orElseThrow(() -> new AssetNotFoundException(code));

        // for sell trade, parameter tradePrice will be -1 and not used for sell orders
        } else {

            return assets.findByCodeAndPortfolioId(code, portfolioId).map(asset -> {

                Portfolio portfolio = portfolioImpl.getPortfolio(portfolioId);

                asset.setQuantity(asset.getQuantity() - changeInQuantity);
                assets.save(asset);

                // update gain/loss of the sold quantity of asset in totalGainLoss of portfolio
                if (asset.getAvgPrice() != tradePrice) {
                    updateTotalGLForSellTrade(portfolio, asset.getAvgPrice(), tradePrice, changeInQuantity);
                }

                return asset;

            }).orElseThrow(() -> new AssetNotFoundException(code));
        }
    }

    public void updateAssetForEditingBuyTrade(Asset asset, int changeInQuantity, double tradePrice) {
        // total value of asset before this trade and total value of asset gotten in this trade
        double oldTotalValue = asset.getQuantity() * asset.getAvgPrice();
        double tradeTotalValue = changeInQuantity * tradePrice;

        // new quantity and avg price of asset after this trade
        int newTotalQuantity = asset.getQuantity() + changeInQuantity;
        double newAvgPrice = (oldTotalValue + tradeTotalValue) / newTotalQuantity;

        asset.setQuantity(newTotalQuantity);
        asset.setAvgPrice(newAvgPrice);
        assets.save(asset);
    }

    public void updateTotalGLForSellTrade(Portfolio portfolio, double assetAvgPrice, double tradePrice, int changeInQuantity) {
        // calculate the difference in avg price of asset and current price of stock of asset
        double changeInPrice = tradePrice - assetAvgPrice;

        // update the totalGainLoss in portfolio by the product of the difference of price and the quantity traded
        portfolio.setTotalGainLoss(portfolio.getTotalGainLoss() + changeInQuantity * changeInPrice);
        portfolios.save(portfolio);
    }

    @Override
    public void deleteAsset(int portfolioId, String code) {
        Portfolio portfolio = portfolioImpl.getPortfolio(portfolioId);

        Asset asset = assets.findByCodeAndPortfolioId(code, portfolioId)
            .orElseThrow(() -> new AssetNotFoundException(code));

        // get the last quantity of asset before it is deleted
        int quantityBeforeDeletion = asset.getQuantity();
        updateTotalGLForSellTrade(portfolio, asset.getAvgPrice(), asset.getCurrentPrice(), quantityBeforeDeletion);

        assets.delete(asset);
    }
}
