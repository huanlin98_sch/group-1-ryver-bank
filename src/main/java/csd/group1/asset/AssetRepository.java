package csd.group1.asset;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssetRepository extends JpaRepository<Asset, Integer> {
    List<Asset> findByPortfolioId(int portfolioId);
    Optional<Asset> findByCodeAndPortfolioId(String code, int portfolioId);
    // void deleteByCodeAndPortfolioId(String code, int portfolioId);
}
