package csd.group1.exception;

public class InactiveUserException extends Exception {

    String username;

    public InactiveUserException(String username) {
        this.username = username;
    }

    public String toString() {
        return "Account with username: " + username + " has been deactivated.";
    }

}
