package csd.group1.txn;

import java.util.List;

public interface TxnService {

    List<Txn> listTxns();

    List<Txn> getAllTxnsByAccountId(int accountId);

    Txn addTxn(Txn txn);
}
