package csd.group1.txn;

import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Txn {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) int id;

    @NotNull
    private int senderId;

    @NotNull
    private int receiverId;

    @NotNull
    private double amount;

    @JsonProperty("from")
    public int getSenderId() {
        return senderId;
    }

    @JsonProperty("to")
    public int getReceiverId() {
        return receiverId;
    }

    public Txn(int senderId, int receiverId, double amount) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.amount = amount;
    }
}
