package csd.group1.txn;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TxnRepository extends JpaRepository<Txn, Integer> {
    List<Txn> findBySenderIdOrReceiverId(int accountId, int accountIdDuplicate);
}
