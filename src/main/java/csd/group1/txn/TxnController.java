package csd.group1.txn;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;

import csd.group1.account.*;
import csd.group1.user.*;

@RestController
public class TxnController {
    private TxnRepository txns;
    private AccountRepository accounts;
    private UserRepository users;

    public TxnController(TxnRepository txns, AccountRepository accounts, UserRepository users) {
        this.txns = txns;
        this.accounts = accounts;
        this.users = users;
    }

    // Throws either 403 forbidden or 404 not_found only.
    @GetMapping("/accounts/{accountId}/transactions")
    public List<Txn> getAllTxnsByAccountId(@PathVariable(value = "accountId") int accountId,
            Authentication authentication) {
        // If user is manager/analyst, no accounts
        if (authentication.getAuthorities().iterator().next().toString().equals("ROLE_MANAGER")
                || authentication.getAuthorities().iterator().next().toString().equals("ROLE_ANALYST")) {
            throw new NoTxnsException();
        }
        User customer = users.findByUsername(authentication.getName()).orElse(null);

        checkAccountAndUser(accountId, customer.getId());
        return txns.findBySenderIdOrReceiverId(accountId, accountId);
    }

    // Throws either 400 bad_request or 403 forbidden only.
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/accounts/{accountId}/transactions")
    public Txn addTxn(@Valid @RequestBody Txn txn, @PathVariable(value = "accountId") int accountId,
            Authentication authentication) {

        if (authentication.getAuthorities().iterator().next().toString().equals("ROLE_MANAGER")
                || authentication.getAuthorities().iterator().next().toString().equals("ROLE_ANALYST")) {
            throw new NoTxnsException();
        }

        // Check if sender account in path and parameters match (400 bad_request if
        // doesn't match)
        if (accountId != txn.getSenderId()) {
            throw new InvalidTxnDetailsException(accountId);
        }

        // Get sender
        User sender = users.findByUsername(authentication.getName()).orElse(null);

        // Check if sender account exists and belongs to customer (400 bad_request if
        // account doesn't exist, 403 forbidden if account does not belong to user)
        Account senderAccount = checkAccountValidity(txn.getSenderId());
        if (senderAccount.getCustomerId() != sender.getId()) {
            throw new AccountForbiddenException(txn.getSenderId());
        }

        // Check if receiver account exists (400 bad_request if doesn't exist)
        Account receiverAccount = checkAccountValidity(txn.getReceiverId());

        // Check if amount is valid (>0)
        if (txn.getAmount() <= 0) {
            throw new InvalidTxnAmountException(txn.getAmount());
        }

        // Check if sender has sufficient available balance
        if (senderAccount.getAvailableBalance() < txn.getAmount()) {
            throw new InsufficientBalanceException(txn.getAmount());
        }

        // Proceed with transaction (deduct from sender, add to receiver)
        senderAccount.updateBothBalances(-txn.getAmount());
        receiverAccount.updateBothBalances(txn.getAmount());

        return txns.save(txn);
    }

    // Use for post request: 400 if account is non-existent
    public Account checkAccountValidity(int accountId) {
        // Check account existence
        Account account = accounts.findById(accountId).orElse(null);
        if (account == null) {
            throw new InvalidTxnDetailsException(accountId);
        }
        return account;
    }

    // 404 not_found if account not found, 403 forbidden if account does not belong
    // to user
    public Account checkAccountAndUser(int accountId, int customerId) {
        // Check account existence
        Account account = accounts.findById(accountId).orElse(null);
        if (account == null) {
            throw new AccountNotFoundException(accountId);
        }
        // Check if account belongs to customer
        if (account.getCustomerId() != customerId) {
            throw new AccountForbiddenException(accountId);
        }
        return account;
    }
}
