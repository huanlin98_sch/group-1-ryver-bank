package csd.group1.txn;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidTxnDetailsException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InvalidTxnDetailsException(int id) {
        super("Invalid transaction details");
    }

}
