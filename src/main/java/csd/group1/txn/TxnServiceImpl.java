package csd.group1.txn;

import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class TxnServiceImpl implements TxnService {
    private TxnRepository txns;

    public TxnServiceImpl(TxnRepository txns) {
        this.txns = txns;
    }

    // To get ALL transactions
    @Override
    public List<Txn> listTxns() {
        return txns.findAll();
    }

    // To get transactions of a particular account
    @Override
    public List<Txn> getAllTxnsByAccountId(int accountId) {
        return txns.findBySenderIdOrReceiverId(accountId, accountId);
    }

    @Override
    public Txn addTxn(Txn txn) {
        return txns.save(txn);
    }
}
