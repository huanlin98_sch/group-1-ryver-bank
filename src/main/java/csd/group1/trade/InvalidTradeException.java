package csd.group1.trade;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidTradeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidTradeException(int id) {
        super("Bad Request");
    }
}