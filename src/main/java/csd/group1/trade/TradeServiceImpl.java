package csd.group1.trade;
import org.springframework.stereotype.Service;

import java.util.*;

import csd.group1.account.*;
import csd.group1.stock.*;
import csd.group1.asset.*;
import csd.group1.txn.InsufficientBalanceException;

@Service
public class TradeServiceImpl implements TradeService {

    private TradeRepository trades;
    private AccountRepository accounts;
    private StockRepository stocks;
    private AssetRepository assets;
    private AssetService assetService;
    private MarketHour marketHour;

    public TradeServiceImpl(StockRepository stocks, TradeRepository trades, AccountRepository accounts,
                            AssetRepository assets, AssetService assetService) {
        this.stocks = stocks;
        this.trades = trades;
        this.accounts = accounts;
        this.assets = assets;
        this.assetService = assetService;
        this.marketHour = new MarketHour(trades);
    }

    @Override
    public Trade getTrade(int id) {
        return trades.findById(id).orElseThrow(() -> new TradeNotFoundException(id));
    }

    @Override
    public Trade updateTrade(int id, Trade newTradeInfo) {
        // this method is only called from tradeController
        // and the only field restclient can update is status (for users to cancel trade if it's open)
        return trades.findById(id).map(trade -> {
            if (newTradeInfo.getStatus().equals("cancelled")) {
                Account userAccount = getAccount(trade);
                userAccount.setAvailableBalance(userAccount.getAvailableBalance() + trade.getAvailBalanceDeducted());
                trade.setStatus(newTradeInfo.getStatus());
            }
            return trades.save(trade);
        }).orElse(null);
    }

    @Override
    public Trade placeBuyOrder(Trade trade) {
        trade = basicTradeVerification(trade);

        if (!marketHour.isOpen()) {
            return placeAfterMarketOrders(trade);
        }

        Stock stock = getStock(trade);
        Integer tradeQuantity = trade.getQuantity();
        Double marketPrice = tradeQuantity * stock.getAsk();

        if (trade.getBid() != 0.0) {
            marketPrice = tradeQuantity * trade.getBid();
        }

        Account account = getAccount(trade);
        checkSufficientBalance(account, marketPrice);

        // update amount of money deducted from avail balance in this trade
        // so if the end amount of money spent on this trade differs from the avail balance deducted initially
        // then will update the avail balance such that it is equal to the amount deducted from balance
        trade.setAvailBalanceDeducted(marketPrice);
        trades.save(trade);
        updateAvailBal(marketPrice, account);

        // market order condition: 0 and Bid >= Ask price
        if (trade.getBid() == 0.0 || Double.compare(trade.getBid(), stock.getAsk()) >= 0) {
            List<Trade> openTrades = getOpenSellTrades(trade, stock);
            placeBuyMatching(openTrades, stock, trade);
            // update new stock info volume has reached 0
            if (stock.getAskVolume() == 0) {
                updateNextLowestAsk(trade, stock);
            }
        } else { // limit order
            // check if the current trade bid price is higher than stock info's bid price
            if (stock.getBid() < trade.getBid()) {
                stock.setBid(trade.getBid());
                stock.setBidVolume(trade.getQuantity());
            }
        }
        if (stock.getBidVolume() == 0) {
            updateNextHighestBid(trade, stock);
        }
        return trades.save(trade);
    }

    @Override
    public Trade placeSellOrder(Trade trade) {
        trade = basicTradeVerification(trade);

        if (!marketHour.isOpen()) {
            return placeAfterMarketOrders(trade);
        }

        String tradeSymbol = trade.getSymbol();
        Stock stock = stocks.findBySymbol(tradeSymbol).orElseThrow(() -> new StockNotFoundException("Invalid Symbol"));

        // Check if sell quantity <= portfolio quantity of seller
        Asset sellerAsset = assets.findByCodeAndPortfolioId(tradeSymbol, trade.getCustomerId())
            .orElseThrow(() -> new InsufficientAssetException(tradeSymbol));
        if (sellerAsset.getQuantity() < trade.getQuantity()) {
            throw new InsufficientAssetException(tradeSymbol);
        }

        // market order condition: 0 and ASK <= BID price
        if (trade.getAsk()== 0.0 || Double.compare(trade.getAsk(), stock.getBid()) <= 0) {
            List<Trade> openTrades = getOpenBuyTrades(trade, stock);
            placeSellMatching(openTrades, stock, trade);

            // if bought out the stock vol
            if (stock.getBidVolume() == 0) {
                updateNextHighestBid(trade, stock);
            }
        } else {
            // check if the current trade bid price is higher than stock info's bid price
            if (stock.getAsk() > trade.getAsk()) {
                updateStockInfoAsk(trade, stock);
            }
        }

        trades.save(trade);
        if (stock.getAskVolume() == 0) {
            updateNextLowestAsk(trade, stock);
        }
        return trades.save(trade);
    }

    public List<Trade> listTrades() {
        return trades.findAll();
    }

    // method used in placeBuyOrder and placeSellOrder methods
    public void updateBuyerAndSellerPortfolios(Trade buyTrade, Trade sellTrade, int tradeQuantity, double buyAvgPrice, Stock stock) {
        // buyAvgPrice is the avg price for ONLY this trade between this buyTrade and sellTrade

        String tradeSymbol = buyTrade.getSymbol();

        // add to buyer's portfolio if customerID of buyer is  not 0 (market maker)
        if (checkValidTrade(buyTrade)) {
            addToBuyerPortfolio(buyTrade.getCustomerId(), tradeSymbol, tradeQuantity, buyAvgPrice);
        }

        // minus from seller's portfolio if customerID of seller is not 0 (market maker)
        if (checkValidTrade(sellTrade)) {
            minusFromSellerPortfolio(sellTrade.getCustomerId(), tradeSymbol, tradeQuantity, buyAvgPrice);
        }
    }

    private boolean checkValidTrade(Trade trade) {
        return trade.getCustomerId() != 0;
    }

    public void addToBuyerPortfolio(int portfolioId, String tradeSymbol, int tradeQuantity, double buyAvgPrice) {
        Asset buyerAsset = findAsset(portfolioId, tradeSymbol);

        // if buyer does not have the asset, add it to portfolio
        if (buyerAsset == null) {
            buyerAsset = new Asset(tradeSymbol, tradeQuantity, buyAvgPrice);
            assetService.addAsset(portfolioId, buyerAsset);

            // else just update the quantity in the portfolio
        } else {
            assetService.editingExistingAsset(portfolioId, tradeSymbol, tradeQuantity, buyAvgPrice, "buy");
        }

    }

    public void minusFromSellerPortfolio(int portfolioId, String tradeSymbol, int tradeQuantity, double buyAvgPrice) {
        Asset sellerAsset = findAsset(portfolioId, tradeSymbol);

        // if seller is selling all his stock in this sellerAsset, then just remove
        // entire asset from his portfolio
        if (sellerAsset.getQuantity() == tradeQuantity) {
            assetService.deleteAsset(portfolioId, tradeSymbol);
        } else {
            // else minus the tradeQuantity from the asset itself
            assetService.editingExistingAsset(portfolioId, tradeSymbol, tradeQuantity, buyAvgPrice, "sell");
        }
    }

    private Asset findAsset(int portfolioId, String tradeSymbol) {
        return assets.findByCodeAndPortfolioId(tradeSymbol, portfolioId).orElse(null);
    }

    private List<Trade> getOpenSellTrades(Trade trade, Stock stock) {
        return trades.findAllBySymbolAndActionAndAskAndStatusIn(trade.getSymbol(), "sell", stock.getAsk(),
            getOpenStatuses());
    }

    private void placeBuyMatching(List<Trade> openTrades, Stock stock, Trade trade) {
        if (!openTrades.isEmpty() && openTrades != null) {
            // get earliest trade
            Trade earliestTrade = openTrades.get(0);
            int marketQuantity = getQtyLeft(earliestTrade);
            double avgPriceOfTrade = stock.getAsk();
            int tradeQuantity = trade.getQuantity();

            // CAN PULL OUT THE UPDATE STOCK AND ACCOUNT BALANCE FROM IF STATEMENTS
            // available quantity to trade
            if (trade.getQuantity() == marketQuantity) { // and account balance >= quantity * ask price
                double tradedPrice = tradeQuantity * avgPriceOfTrade;
                matchBuyTrade(trade, stock, tradeQuantity, earliestTrade, avgPriceOfTrade, tradedPrice, "filled", "filled");

            } else if (tradeQuantity < marketQuantity) {
                double tradedPrice = tradeQuantity * avgPriceOfTrade;
                matchBuyTrade(trade, stock, tradeQuantity, earliestTrade, avgPriceOfTrade, tradedPrice, "filled", "partial-filled");

            } else if (tradeQuantity > marketQuantity) {
                // fill whatever is available for the current market quantity
                double tradedPrice = marketQuantity * avgPriceOfTrade;
                matchBuyTrade(trade, stock, marketQuantity, earliestTrade, avgPriceOfTrade, tradedPrice, "partial-filled", "filled");
                tradeQuantity -= marketQuantity;
                // if partial filled, check for other matches and fill till quantity/balance reaches 0
                // check for trade quantity
                while (tradeQuantity > 0 && !trade.getStatus().equals("filled")) {
                    Account account = getAccount(trade);
                    if (account.getBalance() == 0.0) {
                        break;
                    }
                    openTrades = trades.findAllBySymbolAndActionAndStatusIn(trade.getSymbol(), "sell", getOpenStatuses());
                    Trade lowestAskTrade = getLowestAskTrade(openTrades);
                    int newMarketQuantity = getQtyLeft(lowestAskTrade);
                    int newTradeQty = getAffordableTrade(tradeQuantity, account, lowestAskTrade, newMarketQuantity);

                    avgPriceOfTrade = lowestAskTrade.getAsk();
                    tradedPrice = avgPriceOfTrade * newTradeQty;
                    updateSpecialBuyTradeDetails(trade, stock, avgPriceOfTrade, tradedPrice, lowestAskTrade, newTradeQty);

                    tradeQuantity -= newTradeQty;
                }
            }
        }
    }

    private void matchBuyTrade(Trade trade, Stock stock, Integer tradeQuantity, Trade earliestTrade,
                               double avgPriceOfTrade, double tradedPrice, String newTradeStatus, String newMatchedTradeStatus) {
        updateTradeDetails(trade, earliestTrade, newTradeStatus, newMatchedTradeStatus, tradeQuantity, tradedPrice);
        updateStockAsk(stock, tradeQuantity);

        updateAccountBalance(trade, earliestTrade, tradedPrice);
        updateBuyerAndSellerPortfolios(trade, earliestTrade, tradeQuantity, avgPriceOfTrade, stock);

        setCurrentDateTime(trade);
        setCurrentDateTime(earliestTrade);
    }

    private void updateSpecialBuyTradeDetails(Trade trade, Stock stock, double avgPriceOfTrade, double tradedPrice, Trade lowestAskTrade,
            int newTradeQty) {
        updateSpecialTrade(trade, lowestAskTrade, newTradeQty,tradedPrice);
        saveTrades(trade, lowestAskTrade);

        updateStockAsk(stock, newTradeQty);
        updateAccountBalance(trade, lowestAskTrade, tradedPrice);
        updateBuyerAndSellerPortfolios(trade, lowestAskTrade, newTradeQty, avgPriceOfTrade, stock);
    }

    private void updateNextLowestAsk(Trade trade, Stock stock) {
        List<Trade> openTrades;
        openTrades = trades.findAllBySymbolAndActionAndStatusIn(trade.getSymbol(), "sell", getOpenStatuses());

        if (openTrades != null && !openTrades.isEmpty()) {
            Trade lowestAskTrade = openTrades.get(0);
            for (Trade trde : openTrades) {
                if (Double.compare(trde.getAsk(), lowestAskTrade.getAsk()) < 0) {
                    lowestAskTrade = trde;
                }
            }
            stock.setAsk(lowestAskTrade.getAsk());
            stock.setAskVolume(getQtyLeft(lowestAskTrade));
            stocks.save(stock);
        }
    }

    private void saveTrades(Trade trade, Trade lowestAskTrade) {
        trades.save(trade);
        trades.save(lowestAskTrade);
    }

    private int getQtyLeft(Trade earliestTrade) {
        return earliestTrade.getQuantity() - earliestTrade.getFilledQuantity();
    }

    private void updateAvailBal(double marketPrice, Account account) {
        account.setAvailableBalance(account.getAvailableBalance() - marketPrice);
        accounts.save(account);
    }

    private Account getAccount(Trade trade) {
        return accounts.findById(trade.getAccountId()).orElse(null);
    }

    private Stock getStock(Trade trade) {
        return stocks.findBySymbol(trade.getSymbol())
            .orElseThrow(() -> new StockNotFoundException("Invalid Symbol"));
    }

    private void setCurrentDateTime(Trade trade) {
        Date now = new Date();
        Long unixTime = now.getTime() / 1000L;
        trade.setDate(unixTime);
    }

    private void updateNextHighestBid(Trade trade, Stock stock) {
        List<Trade> openTrades;
        openTrades = trades.findAllBySymbolAndActionAndStatusIn(trade.getSymbol(), "buy", getOpenStatuses());

        if (openTrades != null && !openTrades.isEmpty()) {
            Trade highestBidTrade = openTrades.get(0);
            for (Trade trde : openTrades) {
                if (Double.compare(trde.getBid(), highestBidTrade.getBid()) > 0) {
                    highestBidTrade = trde;
                }
            }
            stock.setBid(highestBidTrade.getBid());
            stock.setBidVolume(getQtyLeft(highestBidTrade));
            stocks.save(stock);
        }
    }

    private void updateSpecialTrade(Trade trade, Trade matchedTrade, int FilledQty, double tradedPrice) {
        double totalCost = getCostOfTrade(trade, tradedPrice);
        int newFilledQty = trade.getFilledQuantity() + FilledQty;        
        trade.setAvgPrice(totalCost / newFilledQty);
        trade.setFilledQuantity(newFilledQty);
        updateStatus(trade);

        // update trade details for matched lowestAskTrade
        newFilledQty = matchedTrade.getFilledQuantity() + FilledQty;
        totalCost = getCostOfTrade(matchedTrade, tradedPrice);
        matchedTrade.setAvgPrice(totalCost / newFilledQty);
        matchedTrade.setFilledQuantity(newFilledQty);
        updateStatus(matchedTrade);
    }

    private void updateStatus(Trade lowestAskTrade) {
        if (lowestAskTrade.getFilledQuantity() == lowestAskTrade.getQuantity()){
            lowestAskTrade.setStatus("filled");
        } else {
            lowestAskTrade.setStatus("partial-filled");
        }
    }

    private double getCostOfTrade(Trade trade, double tradedPrice) {
        return trade.getAvgPrice() * trade.getFilledQuantity() + tradedPrice;
    }

    private Trade getLowestAskTrade(List<Trade> openTrades) {
        Trade lowestAskTrade = openTrades.get(0);
        if (openTrades != null && !openTrades.isEmpty()) {
            // get lowest bid among all the openTrades
            for (Trade trade : openTrades) {
                if (Double.compare(trade.getAsk(), lowestAskTrade.getAsk()) < 0) {
                    lowestAskTrade = trade;
                }
            }
        }
        return lowestAskTrade;
    }

    private void updateStockAsk(Stock stock, int tradeQuantity) {
        if (stock.getAskVolume() - tradeQuantity <= 0) {
            stock.setAskVolume(0);
        } else {
            stock.setAskVolume(stock.getAskVolume() - tradeQuantity);
        }
        stock.setLastPrice(stock.getAsk());
    }

    private void updateStockBid(Stock stock, int marketQuantity) {
        stock.setBidVolume(stock.getBidVolume() - marketQuantity);
        stock.setLastPrice(stock.getBid());
    }

    private void updateAccountBalance(Trade buyTrade, Trade sellTrade, double marketPrice) {

        if (buyTrade.getAccountId() != 0) {
            Account buyerAccount = accounts.findById(buyTrade.getAccountId())
                .orElseThrow(() -> new AccountForbiddenException(buyTrade.getAccountId()));

            buyerAccount.setBalance(buyerAccount.getBalance() - marketPrice);

            // update available balance if end sum spent on this trade is more or less than
            // what was deducted previously
            // for market orders
            if (buyTrade.getStatus().equals("filled")) {
                double finalSumSpentOnTrade = buyTrade.getAvgPrice() * buyTrade.getQuantity();
                double difference = finalSumSpentOnTrade - buyTrade.getAvailBalanceDeducted();
                buyerAccount.setAvailableBalance(buyerAccount.getAvailableBalance() - difference);
            }

            accounts.save(buyerAccount);
        }

        if (sellTrade.getAccountId() != 0) {
            Account sellerAccount = accounts.findById(sellTrade.getAccountId())
                .orElseThrow(() -> new AccountForbiddenException(sellTrade.getAccountId()));

            sellerAccount.updateBothBalances(marketPrice);
            accounts.save(sellerAccount);
        }
    }

    private List<String> getOpenStatuses() {
        List<String> status = new ArrayList<String>();
        status.add("open");
        status.add("partial-filled");
        return status;
    }

    public void updateTradeDetails(Trade trade, Trade matchedTrade, String statusTrade, String statusMatchedTrade,
                                   int tradedQuantity, double tradedPrice) {
        trade = setTradeInfo(trade, tradedQuantity, tradedPrice, statusTrade);
        matchedTrade = setTradeInfo(matchedTrade, tradedQuantity, tradedPrice, statusMatchedTrade);
        saveTrades(trade, matchedTrade);
    }

    private Trade setTradeInfo(Trade trade, Integer tradedQuantity, Double tradedPrice, String statusTrade) {
        if (trade.getStatus().equals("partial-filled")) {
            int newFilledQty = trade.getFilledQuantity() + tradedQuantity;
            double totalCost = getCostOfTrade(trade, tradedPrice);

            trade.setAvgPrice(totalCost / newFilledQty);
            trade.setFilledQuantity(newFilledQty);
        } else {
            trade.setAvgPrice(tradedPrice / tradedQuantity);
            trade.setFilledQuantity(tradedQuantity);
        }
        trade.setStatus(statusTrade);
        return trade;
    }

    private void updateStockInfoAsk(Trade trade, Stock stock) {
        stock.setAsk(trade.getAsk());
        stock.setAskVolume(trade.getQuantity());
    }

    private List<Trade> getOpenBuyTrades(Trade trade, Stock stock) {
        return trades.findAllBySymbolAndActionAndBidAndStatusIn(trade.getSymbol(), "buy", stock.getBid(), getOpenStatuses());
    }

    private void placeSellMatching(List<Trade> openTrades, Stock stock, Trade trade) {
        if (!openTrades.isEmpty() && openTrades != null) {
            // get earliest trade
            Trade earliestTrade = openTrades.get(0);
            Integer marketQuantity = getQtyLeft(earliestTrade);
            Integer tradeQuantity = trade.getQuantity();
            double avgPriceOfTrade = stock.getBid();

            // available quantity to trade
            if (trade.getQuantity() == marketQuantity) {
                double tradedPrice = tradeQuantity * avgPriceOfTrade;
                matchSellTrade(trade, stock, earliestTrade, "filled", "filled", tradeQuantity, avgPriceOfTrade, tradedPrice);

            } else if (tradeQuantity < marketQuantity) {
                double tradedPrice = tradeQuantity * avgPriceOfTrade;
                matchSellTrade(trade, stock, earliestTrade, "filled", "partial-filled", tradeQuantity, avgPriceOfTrade, tradedPrice);

            } else if (tradeQuantity > marketQuantity) {
                double tradedPrice = tradeQuantity * avgPriceOfTrade;
                matchSellTrade(trade, stock, earliestTrade, "partial-filled", "filled", marketQuantity, avgPriceOfTrade, tradedPrice);
                tradeQuantity -= marketQuantity;

                // check for any other open trades to match
                while (tradeQuantity > 0 && !trade.getStatus().equals("filled")) {
                    Account account = getAccount(trade);
                    if (account.getBalance() == 0.0) {
                        break;
                    }
                    openTrades = trades.findAllBySymbolAndActionAndStatusIn(trade.getSymbol(), "buy", getOpenStatuses());
                    Trade highestBidTrade = getHighestBidTrade(openTrades);

                    int newMarketQuantity = getQtyLeft(highestBidTrade);
                    Integer newTradeQty = getAffordableTrade(tradeQuantity, account, highestBidTrade, newMarketQuantity);

                    avgPriceOfTrade = highestBidTrade.getBid();
                    tradedPrice = avgPriceOfTrade * newTradeQty;

                    updateSpecialSellTrade(trade, stock, avgPriceOfTrade, tradedPrice, highestBidTrade, newTradeQty);
                    tradeQuantity -= newTradeQty;
                }
            }
        }
    }

    private int getAffordableTrade(int tradeQuantity, Account account, Trade matchedTrade, int newMarketQuantity) {
        int newTradeQty = (int) Math.min(Math.floor(account.getBalance() / matchedTrade.getAsk()), newMarketQuantity) ;
           newTradeQty = (int) Math.min(newTradeQty, tradeQuantity);
        return newTradeQty;
    }

    private void updateSpecialSellTrade(Trade trade, Stock stock, double avgPriceOfTrade, double tradedPrice, Trade highestBidTrade,
            int newTradeQty) {
        updateSpecialTrade(trade, highestBidTrade, newTradeQty, tradedPrice);
        saveTrades(trade, highestBidTrade);

        updateAccountBalance(highestBidTrade, trade, tradedPrice);
        updateBuyerAndSellerPortfolios(highestBidTrade, trade, newTradeQty, avgPriceOfTrade, stock);
    }

    private Trade getHighestBidTrade(List<Trade> openTrades) {
        Trade highestBidTrade = openTrades.get(0);
        if (openTrades != null && !openTrades.isEmpty()) {
            // get highest bid each time
            for (Trade trde : openTrades) {
                if (Double.compare(trde.getBid(), highestBidTrade.getBid()) > 0) {
                    highestBidTrade = trde;
                }
            }
        }
        return highestBidTrade;
    }

    private void matchSellTrade(Trade trade, Stock stock, Trade earliestTrade, String tradeStatus, String matchedTradeStatus, Integer tradeQuantity, double avgPriceOfTrade,
                                double tradedPrice) {
        updateTradeDetails(trade, earliestTrade, tradeStatus, matchedTradeStatus, tradeQuantity, tradedPrice);
        updateStockBid(stock, tradeQuantity);

        updateAccountBalance(earliestTrade, trade, tradedPrice);
        updateBuyerAndSellerPortfolios(earliestTrade, trade, tradeQuantity, avgPriceOfTrade, stock);

        setCurrentDateTime(trade);
        setCurrentDateTime(earliestTrade);
    }

    @Override
    public Trade placeAfterMarketOrders(Trade trade) {
        if (trade.getQuantity() % 100 != 0) {
            throw new NotMultiplesOfHundredException(trade.getQuantity());
        }
        Stock stock = getStock(trade);

        // check for account balance
        Account account = getAccount(trade);
        if (trade.getAction().equals("buy")) {

            Integer tradeQuantity = trade.getQuantity();
            Double marketPrice = tradeQuantity * stock.getAsk();
            checkSufficientBalance(account, marketPrice);

            Double tradePrice = tradeQuantity * stock.getAsk();
            updateAvailBal(tradePrice, account);
        } else {

            String tradeSymbol = trade.getSymbol();
            Asset sellerAsset = assets.findByCodeAndPortfolioId(tradeSymbol, trade.getCustomerId())
                .orElseThrow(() -> new InsufficientAssetException(tradeSymbol));
            if (sellerAsset.getQuantity() < trade.getQuantity()) {
                throw new InsufficientAssetException(tradeSymbol);
            }
        }
        setCurrentDateTime(trade);

        trades.save(trade);
        return trades.save(trade);
    }

    private Trade basicTradeVerification(Trade trade) {
        trade.setStatus("open");
        if (trade.getQuantity() % 100 != 0) {
            throw new NotMultiplesOfHundredException(trade.getQuantity());
        }
        return trade;
    }

    private void checkSufficientBalance(Account account, double marketPrice) {
        Double accountBalance = account.getAvailableBalance();
        if (accountBalance < marketPrice) {
            throw new InsufficientBalanceException(marketPrice);
        }
    }
}