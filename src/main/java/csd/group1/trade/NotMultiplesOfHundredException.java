package csd.group1.trade;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST) // 400 Error
public class NotMultiplesOfHundredException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NotMultiplesOfHundredException(int quantity) {
        super("Quantity is not in multiples of 100 ");
    }
}
