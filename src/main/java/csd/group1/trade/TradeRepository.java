package csd.group1.trade;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TradeRepository extends JpaRepository<Trade, Integer>{
    // additional derived queries specified here will be implemented by Spring Data JPA
    // start the derived query with "findBy", then reference the entity attributes you want to filter
    Trade findBySymbolAndActionAndAccountId(String symbol, String action, int accountId);
    List<Trade> findAllBySymbol(String symbol);
    //List<Trade> findAllBySymbolAndActionAndStatus(String symbol, String action, String status);
    List<Trade> findAllBySymbolAndActionAndAskAndStatusIn(String symbol, String action, double bid, List<String> status);
    List<Trade> findAllBySymbolAndActionAndBidAndStatusIn(String symbol, String action, double ask, List<String> status);
    // Optional<Review> findByIdAndBookId(Long id, Long bookId);
    List<Trade> findAllBySymbolAndActionAndStatusIn(String symbol, String action, List<String> status);

    List<Trade> findAllByStatusIn(List<String> status);
    Trade findByAccountId(int accountId);
}
