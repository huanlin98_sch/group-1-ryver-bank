package csd.group1.trade;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST) // 400 Error
public class MarketNotOpenException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public MarketNotOpenException() {
        super("Market only opens from 9:00AM - 5:00PM daily.");
    }
}
