package csd.group1.trade;

import java.util.List;

public interface TradeService {
    List<Trade> listTrades();
    //List<Trade> listAllTradesByStockId(String symbol);
    Trade placeSellOrder(Trade trade);
    Trade placeBuyOrder(Trade trade);
    //Trade addTrade(Trade trade);
    Trade updateTrade(int id, Trade newTradeInfo);
    Trade getTrade(int id);
    Trade placeAfterMarketOrders(Trade trade);
	void addToBuyerPortfolio(int customerID, String string, int i, double d);
}
