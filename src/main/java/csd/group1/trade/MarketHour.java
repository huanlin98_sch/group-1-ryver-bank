package csd.group1.trade;

import java.util.*;

public class MarketHour {
  private TradeRepository trades;

  public MarketHour (TradeRepository trades) {
    this.trades = trades;
  }

  public boolean isOpen() {
    TimeZone.setDefault(TimeZone.getTimeZone("Asia/Singapore"));
    TimeZone timeZone = TimeZone.getTimeZone("Asia/Singapore");

    Calendar currTime = Calendar.getInstance(timeZone);

    if (currTime.get(Calendar.DAY_OF_WEEK) < Calendar.MONDAY || currTime.get(Calendar.DAY_OF_WEEK) > Calendar.FRIDAY  ){
      return false;
    }
    Calendar openingHour = Calendar.getInstance(timeZone);
    openingHour.set(Calendar.HOUR_OF_DAY, 9);
    openingHour.set(Calendar.MINUTE, 0);
    openingHour.set(Calendar.SECOND, 0);

    Calendar closingHour = Calendar.getInstance(timeZone);
    closingHour.set(Calendar.HOUR_OF_DAY, 17);
    closingHour.set(Calendar.MINUTE, 1);
    closingHour.set(Calendar.SECOND, 0);

    Date x = currTime.getTime();

    //checks whether the current time is between 9am - 5pm.
    if (!(x.after(openingHour.getTime()) && x.before(closingHour.getTime()))) {
      List<Trade> openTrades = trades.findAllByStatusIn(getOpenStatuses());

      //expire orders for orders made before 5pm
      if (openTrades != null ) {
        ExpireOrders(currTime, closingHour);
      }

      return false;
    }
    return true;
  }

  private List<String> getOpenStatuses() {
    List<String> status = new ArrayList<String>();
    status.add("open");
    status.add("partial-filled");
    return status;
  }

  public void ExpireOrders(Calendar now, Calendar closingHour) {
    List<Trade> openTrades = findOpenTrades();
    if (openTrades == null){
      return;
    }
    for (Trade trade : openTrades) {
      Date tradingDateTime = new java.util.Date(trade.getDate() * 1000L);
      if (tradingDateTime.before(closingHour.getTime())){
        trade.setStatus("expired");
      }
      trades.save(trade);
    }
  }

  public List<Trade> findOpenTrades () {
    return trades.findAllByStatusIn(getOpenStatuses());
  }
}
