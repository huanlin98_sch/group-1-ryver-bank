package csd.group1.trade;

// external lib
import lombok.*;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@Entity
@Getter
@Setter
@ToString
//@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode

// to do: add in validation for each field
// for buy trades, check if customer bank acc has enough balance, deduct from customers bank account
// for sell trades, make sure customer have enough balance
// update stock info after each trade is successful

public class Trade {
    @JsonProperty("id") 
    private @Id @GeneratedValue (strategy = GenerationType.IDENTITY) int tradeId;

    @JsonProperty("action")
    private String action;

    @JsonProperty("symbol")
    private String symbol;

    @JsonProperty("quantity")
    private int quantity;

    @JsonProperty("bid")
    private double bid;
    @JsonProperty("ask")
    private double ask;

    @JsonProperty("avg_price")
    private double avgPrice = 0.0;

    @JsonProperty("filled_quantity")
    private int filledQuantity = 0;

    @JsonProperty("date")
    private Long date; //timestamp expires after 5pm, use now.getTime() on Date object

    // Many trades to one account
    @JsonProperty("account_id")
    private int accountId;

    @JsonProperty("customer_id")
    private int customerId;
    
    @JsonProperty("status")
    private String status = "open";

    @JsonIgnore
    private double availBalanceDeducted = 0.0;

    //public int getTradeId() { return tradeId; }
    
    public Trade(String symbol, String action, int quantity, int customer_id, int account_id, double bid, double ask, String status) {
        this.action = action;
        this.symbol = symbol;
        this.quantity = quantity;
        this.filledQuantity = 0; 
        this.customerId = customer_id;
        this.accountId = account_id;
        this.bid= bid;
        this.ask = ask;
        this.status = "open";
        Date now = new Date();
        Long unixTime = now.getTime() / 1000L;
        this.date = unixTime;
    }

}