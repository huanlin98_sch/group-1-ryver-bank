package csd.group1.trade;

import java.util.*;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.security.*;

import csd.group1.account.*;
import csd.group1.user.*;

@RestController
public class TradeController {
    private TradeService tradeService;
    private UserRepository users;
    private AccountRepository accounts;

    public TradeController(TradeService tradeService, UserRepository users, AccountRepository accounts){
        this.tradeService = tradeService;
        this.users = users;
        this.accounts = accounts;
    }

    @GetMapping("/trades")
    public List<Trade> getTrades() {
        return tradeService.listTrades();
    }

    // GET: Throws either 403 forbidden or 404 not_found only.
    @GetMapping("/trades/{id}") 
    public Trade getTrade(@PathVariable(value = "id") Integer id, Principal principal) {
        Trade trade = basicTradeChecks(id, principal);

        return trade;
    }

    // CHECK AGAIN: Throws either 404 not_found or 403 forbidden
    @PutMapping("/trades/{id}") 
    public Trade updateTradeToCancelled(@PathVariable(value = "id") Integer id, Principal principal, @RequestBody Trade newTradeInfo) {
        Trade trade = basicTradeChecks(id, principal);

        return tradeService.updateTrade(id, newTradeInfo);
    }

    // POST: Throws either 400 bad_request or 403 forbidden only.
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/trades")
    public Trade addTrade(@RequestBody Trade trade, Principal principal) {
        tradeCreationChecks(trade, principal);
        
        if (trade.getAction().equals("buy")){
            return tradeService.placeBuyOrder(trade);
        }
        return tradeService.placeSellOrder(trade);
    }

    private Trade basicTradeChecks(Integer id, Principal principal) {
        User customer = users.findByUsername(principal.getName()).orElse(null);

        Trade trade = tradeService.getTrade(id);
        if (trade == null) {
            throw new TradeNotFoundException(id);
        }

        // Check if trade belongs to this user (403 forbidden if does not belong)
        if (trade.getCustomerId() != customer.getId()) {
            throw new TradeForbiddenException(id);
        }
        return trade;
    }

    private void tradeCreationChecks (Trade trade, Principal principal) {
        // Check if customerId in parameter exists (400 bad_request if customer doesn't exist)
        User customer = users.findById(trade.getCustomerId()).orElse(null);
        if (customer == null) {
            throw new InvalidTradeException(trade.getCustomerId());
        }

        // Check if customerId in trade parameters belongs to principal (403 forbidden if id does not belong to user)
        User principalCustomer = users.findByUsername(principal.getName()).orElse(null);
        if (principalCustomer.getId() != customer.getId()) {
            throw new TradeForbiddenException(trade.getTradeId());
        }

        // Check if trade account exists (400 bad_request if not)
        Account account = accounts.findById(trade.getAccountId()).orElse(null);
        if (account == null) {
            throw new InvalidTradeException(trade.getAccountId());
        }

        // Check if trade account belongs to principal (403 forbidden if does not belong)
        if (account.getCustomerId() != customer.getId()) {
            throw new AccountForbiddenException(account.getId());
        }
    }
}