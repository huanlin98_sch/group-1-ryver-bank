package csd.group1.content;

import java.util.List;

public interface ContentService {
    List<Content> listContents();

    List<Content> listApprovedContents();

    Content getContent(int id);

    Content addContent(Content content);

    Content updateContent(int id, Content content);

    Content analystContentUpdate(int id, Content content);

    void deleteContent(int id);
}