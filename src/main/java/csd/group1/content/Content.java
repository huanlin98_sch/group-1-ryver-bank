package csd.group1.content;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode

public class Content {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) int id;

    @NotNull(message = "Title should not be null")
    @Size(min = 5, max = 200, message = "Title should be at least 5 characters long")
    private String title;

    @NotNull(message = "Summary should not be null")
    @Size(min = 5, max = 200, message = "Summary should be at least 5 characters long")
    private String summary;

    @NotNull(message = "Content should not be null")
    @Size(min = 5, max = 200, message = "Content should be at least 5 characters long")
    private String content;
    private String link = null;
    private boolean approved = false;

    public Content(String title, String summary, String content, String link) {
        this.title = title;
        this.summary = summary;
        this.content = content;
        this.link = link;
    }
}