package csd.group1.content;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN) // 403 Error
public class ContentUnauthorizedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ContentUnauthorizedException(int id) {
        super("Content requested is unauthorized");
    }

}
