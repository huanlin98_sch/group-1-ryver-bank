package csd.group1.content;

import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ContentServiceImpl implements ContentService {
    private ContentRepository contents;

    public ContentServiceImpl(ContentRepository contents) {
        this.contents = contents;
    }

    @Override
    public List<Content> listContents() {
        return contents.findAllByOrderByIdDesc();
    }

    @Override
    public List<Content> listApprovedContents() {
        return contents.findAllByApprovedOrderByIdDesc(true);
    }

    @Override
    public Content getContent(int id) {
        return contents.findById(id).map(content -> {
            return content;
        }).orElse(null);
    }

    @Override
    public Content addContent(Content content) {
        return contents.save(content);
    }

    @Override
    public Content updateContent(int id, Content newContentInfo) {
        return contents.findById(id).map(content -> {
            content = basicContentSetting(content, newContentInfo);
            content.setApproved(newContentInfo.isApproved());
            return contents.save(content);
        }).orElse(null);
    }

    @Override
    public Content analystContentUpdate(int id, Content newContentInfo) {
        return contents.findById(id).map(content -> {
            content = basicContentSetting(content, newContentInfo);
            return contents.save(content);
        }).orElse(null);
    }

    @Override
    public void deleteContent(int id) {
        contents.deleteById(id);
    }

    private Content basicContentSetting (Content content, Content newContentInfo) {
        content.setTitle(newContentInfo.getTitle());
        content.setSummary(newContentInfo.getSummary());
        content.setContent(newContentInfo.getContent());
        content.setLink(newContentInfo.getLink());
        return content;
    }

}