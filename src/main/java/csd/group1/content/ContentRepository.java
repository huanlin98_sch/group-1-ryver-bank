package csd.group1.content;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContentRepository extends JpaRepository<Content, Integer> {
    List<Content> findAllByOrderByIdDesc();

    List<Content> findAllByApprovedOrderByIdDesc(boolean approved);
}