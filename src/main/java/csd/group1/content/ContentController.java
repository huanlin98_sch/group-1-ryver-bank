package csd.group1.content;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContentController {
    private ContentService contentService;

    public ContentController(ContentService cs) {
        this.contentService = cs;
    }

    @GetMapping("/contents")
    public List<Content> getContents(Authentication authentication) {

        if (roleChecker("ROLE_USER", authentication)) {
            return contentService.listApprovedContents();

        } else {
            return contentService.listContents();
        }
    }

    @GetMapping("/contents/{id}")
    public Content getContent(@PathVariable int id, Authentication authentication) {
        Content content = contentService.getContent(id);
        if (content == null)
            throw new ContentNotFoundException(id);

        if (roleChecker("ROLE_USER", authentication)) {
            if (content.isApproved()) {
                return content;
            }
            throw new ContentNotFoundException(id);
        }
        return content;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/contents")
    public Content addContent(@RequestBody Content content, Authentication authentication) {
        if (roleChecker("ROLE_ANALYST", authentication))  {
            content.setApproved(false);
        }
        return contentService.addContent(content);
    }

    @PutMapping("/contents/{id}")
    public Content updateContent(@PathVariable int id, @RequestBody Content newContentInfo,
            Authentication authentication) {

        Content content = contentService.getContent(id);
        if (content == null)
            throw new ContentNotFoundException(id);

        if (roleChecker("ROLE_MANAGER", authentication)) {
            content = contentService.updateContent(id, newContentInfo);
        } else {
            content = contentService.analystContentUpdate(id, newContentInfo);
        }

        return content;
    }

    @DeleteMapping("/contents/{id}")
    public void deleteContent(@PathVariable int id) {
        try {
            contentService.deleteContent(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ContentNotFoundException(id);
        }
    }

    private boolean roleChecker (String expectedRole, Authentication authentication) {
        return expectedRole.equals(authentication.getAuthorities().iterator().next().toString());
    }
}