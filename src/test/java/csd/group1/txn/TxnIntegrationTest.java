package csd.group1.txn;

import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import csd.group1.user.*;
import csd.group1.account.*;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TxnIntegrationTest {
    @LocalServerPort
    private int port;
    private final String baseUrl = "http://localhost:";

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private TxnRepository txns;
    @Autowired
    private AccountRepository accounts;
    @Autowired
    private UserRepository users;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @AfterEach
    void tearDown() {
        txns.deleteAll();
        accounts.deleteAll();
        users.deleteAll();
    }

    // (user get transactions of one of his accounts -- success)
    @Test
    public void getAllTxnsByAccountId_ValidUser_Success() throws Exception {
        //create user
        int customerId = users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER")).getId();

        //create 2 accounts for user1, one sender and one receiver
        Account sender = new Account(customerId, 1000.0, 1000.0);
        Account receiver = new Account(customerId, 1000.0, 1000.0);
        Integer senderId = accounts.save(sender).getId();
        Integer receiverId = accounts.save(receiver).getId();

        //create 2 transactions from account1 to account2
        txns.save(new Txn(senderId, receiverId, 100.0));
        txns.save(new Txn(senderId, receiverId, 100.0));

        URI uri = new URI(baseUrl + port + "/accounts/" + senderId + "/transactions");

        ResponseEntity<Txn[]> result = restTemplate.withBasicAuth("user1", "goodpassword").getForEntity(uri, Txn[].class);
        Txn[] txn = result.getBody();

        assertEquals(200, result.getStatusCode().value());
        assertEquals(2, txn.length);
    }

    // addTxn (all valid -- success)
    @Test
    public void addTxn_ValidSender_ValidReceiver_ValidAmount_Success() throws Exception {
        //create user
        int customerId = users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER")).getId();

        //create 2 accounts for user1, one sender and one receiver
        Account sender = new Account(customerId, 1000.0, 1000.0);
        Account receiver = new Account(customerId, 1000.0, 1000.0);
        Integer senderId = accounts.save(sender).getId();
        Integer receiverId = accounts.save(receiver).getId();

        //create txn object
        Txn txn = new Txn(senderId, receiverId, 100.0);

        URI uri = new URI(baseUrl + port + "/accounts/" + senderId + "/transactions");

        ResponseEntity<Txn> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri, txn, Txn.class);

        assertEquals(201, result.getStatusCode().value());
        assertEquals(txn.getSenderId(), result.getBody().getSenderId());
        assertEquals(txn.getReceiverId(), result.getBody().getReceiverId());
        assertEquals(txn.getAmount(), result.getBody().getAmount());
        
        sender = accounts.findById(senderId).orElse(null);
        receiver = accounts.findById(receiverId).orElse(null);
        assertEquals(900.0, sender.getAvailableBalance());
        assertEquals(900.0, sender.getBalance());
        assertEquals(1100.0, receiver.getAvailableBalance());
        assertEquals(1100.0, receiver.getBalance());
    }

    // addTxn (sender has insufficient balance -- 400 failure)
    @Test
    public void addTxn_InvalidSender_ValidReceiver_ValidAmount_Failure() throws Exception {
        //create user
        int customerId = users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER")).getId();

        //create 2 accounts for user1, one sender and one receiver
        Account sender = new Account(customerId, 1000.0, 1000.0);
        Account receiver = new Account(customerId, 1000.0, 1000.0);
        Integer senderId = accounts.save(sender).getId();
        Integer receiverId = accounts.save(receiver).getId();

        //create txn object
        Txn txn = new Txn(senderId, receiverId, 2000.0);

        URI uri = new URI(baseUrl + port + "/accounts/" + senderId + "/transactions");

        ResponseEntity<Txn> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri, txn, Txn.class);

        assertEquals(400, result.getStatusCode().value());
    }
    
    // addTxn (receiver account does not exist -- 400 failure)
    @Test
    public void addTxn_ValidSender_InvalidReceiver_ValidAmount_Failure() throws Exception {
        //create user
        int customerId = users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER")).getId();

        //create 2 accounts for user1, one sender and one receiver
        Account sender = new Account(customerId, 1000.0, 1000.0);
        Integer senderId = accounts.save(sender).getId();

        //create txn object
        Txn txn = new Txn(senderId, 2342224, 100.0);

        URI uri = new URI(baseUrl + port + "/accounts/" + senderId + "/transactions");

        ResponseEntity<Txn> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri, txn, Txn.class);

        assertEquals(400, result.getStatusCode().value());
    }

    // addTxn (negative transaction amount -- 400 failure)
    @Test
    public void addTxn_ValidSender_ValidReceiver_InvalidAmount_Failure() throws Exception {
        //create user
        int customerId = users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER")).getId();

        //create 2 accounts for user1, one sender and one receiver
        Account sender = new Account(customerId, 1000.0, 1000.0);
        Account receiver = new Account(customerId, 1000.0, 1000.0);
        Integer senderId = accounts.save(sender).getId();
        Integer receiverId = accounts.save(receiver).getId();

        //create txn object
        Txn txn = new Txn(senderId, receiverId, -100.0);

        URI uri = new URI(baseUrl + port + "/accounts/" + senderId + "/transactions");


        ResponseEntity<Txn> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri, txn, Txn.class);

        assertEquals(400, result.getStatusCode().value());
    }

    @Test
    public void addTxn_InvalidSender_ValidReceiver_ValidAmount_Failure2() throws Exception {
        //create user
        int customerId = users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER")).getId();

        //create 2 accounts for user1, one sender and one receiver
        // Account sender = new Account(customerId, 1000.0, 1000.0);
        Account receiver = new Account(customerId, 1000.0, 1000.0);
        // Integer senderId = accounts.save(sender).getId();
        Integer receiverId = accounts.save(receiver).getId();

        //create txn object
        Txn txn = new Txn(2342224, receiverId, 100.0);

        URI uri = new URI(baseUrl + port + "/accounts/" + 2342224 + "/transactions");


        ResponseEntity<Txn> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri, txn, Txn.class);

        assertEquals(400, result.getStatusCode().value());
    }
}
