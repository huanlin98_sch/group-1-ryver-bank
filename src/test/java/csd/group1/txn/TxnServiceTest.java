package csd.group1.txn;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TxnServiceTest {

    @Mock
    private TxnRepository txns;

    @InjectMocks
    private TxnServiceImpl txnServiceImpl;

    @Test
    void addTxn_NewTxn_ReturnSavedTxn() {
        Txn txn = new Txn(1, 2, 1000.0);
        
        when(txns.save(any(Txn.class))).thenReturn(txn);

        Txn savedTxn = txnServiceImpl.addTxn(txn);

        assertNotNull(savedTxn);
        verify(txns).save(txn);
    }
}
