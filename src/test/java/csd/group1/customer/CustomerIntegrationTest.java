package csd.group1.customer;

import csd.group1.user.User;
import csd.group1.user.UserRepository;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.net.URI;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerIntegrationTest {
  @LocalServerPort
  private int port;
  private final String baseUrl = "http://localhost:";

  @Autowired
  private TestRestTemplate restTemplate;
  @Autowired
  private UserRepository users;
  @Autowired
  private BCryptPasswordEncoder encoder;

  String username = "good_user_1";
  String password = "01_user_01";
  String validNric = "S1234567D";
  String validPhone = "98765432";
  String address = "white house";
  String newPassword = "newpassword";
  String fullName = "Charlie Brown";
  Boolean active = false;
  String originalRole = "ROLE_USER";

  String invalidNric = "S1234567C";
  String invalidPhone = "76543218";

  private void createManager() {
    users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));
  }

  private void createAnalyst() {
    users.save(new User("analyst_1", encoder.encode("01_analyst_01"), "ROLE_ANALYST"));
  }

  private int createBasicUser() {
    return users.save(new User(username, encoder.encode(password), originalRole)).getId();
  }

  private HttpEntity<String> createCustomerRequest() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    JSONObject userJsonObject = new JSONObject();
    userJsonObject.put("username", username);
    userJsonObject.put("password", password);
    userJsonObject.put("authorities", originalRole);
    return new HttpEntity<String>(userJsonObject.toString(), headers);
  }

  private HttpEntity<String> validUpdateCustomerRequest() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    JSONObject userJsonObject = new JSONObject();
    userJsonObject.put("username", username);
    userJsonObject.put("password", newPassword);
    userJsonObject.put("authorities", originalRole);
    userJsonObject.put("full_name", fullName);
    userJsonObject.put("address", address);
    userJsonObject.put("nric", validNric);
    userJsonObject.put("phone", validPhone);
    userJsonObject.put("active", active);
    return new HttpEntity<String>(userJsonObject.toString(), headers);
  }

  private Map<String, Object> convertResponseToMap(ResponseEntity<String> response) {
    String json = response.getBody();
    ObjectMapper mapper = new ObjectMapper();
    Map<String, Object> result = null;
    try {
      result = mapper.readValue(json, Map.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  @AfterEach
  void tearDown() {
    // clear the database after each test
    users.deleteAll();
  }

  @Test
  public void addCustomer_Manager_Success() throws Exception {
    URI uri = new URI(baseUrl + port + "/customers");
    createManager();

    ResponseEntity<String> response = restTemplate.withBasicAuth("manager", "goodpassword")
        .postForEntity(uri, createCustomerRequest(), String.class);

    Map<String, Object> map = convertResponseToMap(response);

    assertEquals(201, response.getStatusCode().value());
    assertEquals(username, map.get("username"));
    assertNotEquals(password, map.get("password"));
  }

  @Test
  public void addCustomer_Manager_SameUsername_Failure() throws Exception {
    URI uri = new URI(baseUrl + port + "/customers");
    createManager();
    createBasicUser();

    ResponseEntity<String> response = restTemplate.withBasicAuth("manager", "goodpassword")
        .postForEntity(uri, createCustomerRequest(), String.class);

    Map<String, Object> map = convertResponseToMap(response);

    assertEquals(409, response.getStatusCode().value());
  }

  @Test
  public void viewCustomerInfo_Customer_Success() throws Exception {
    int id = createBasicUser();
    URI uri = new URI(baseUrl + port + "/customers/" + id);

    ResponseEntity<String> response = restTemplate.withBasicAuth(username, password).getForEntity(uri,
        String.class);

    assertEquals(200, response.getStatusCode().value());
  }

  @Test
  public void viewCustomerInfo_AnotherCustomer_Failure() throws Exception {
    int id = createBasicUser();
    users.save(new User("another customer", encoder.encode("password"), "ROLE_USER"));
    URI uri = new URI(baseUrl + port + "/customers/" + id);

    ResponseEntity<String> response = restTemplate.withBasicAuth("another customer", "password").getForEntity(uri,
        String.class);

    assertEquals(403, response.getStatusCode().value());
  }

  @Test
  public void updateCustomerInfo_Manager_Success() throws Exception {
    int id = createBasicUser();
    createManager();
    URI uri = new URI(baseUrl + port + "/customers/" + id);

    ResponseEntity<String> response = restTemplate.withBasicAuth("manager", "goodpassword")
        .exchange(uri, HttpMethod.PUT, validUpdateCustomerRequest(), String.class);

    Map<String, Object> map = convertResponseToMap(response);

    assertEquals(200, response.getStatusCode().value());
    assertEquals(address, map.get("address"));
//    assertEquals(encoder.encode(password), map.get("password"));
    assertEquals(validNric, map.get("nric"));
    assertEquals(validPhone, map.get("phone"));
    assertEquals(active, map.get("active"));
  }

  private HttpEntity<String> invalidUpdateCustomerRequest() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    JSONObject userJsonObject = new JSONObject();
    userJsonObject.put("username", username);
    userJsonObject.put("password", newPassword);
    userJsonObject.put("authorities", originalRole);
    userJsonObject.put("full_name", fullName);
    userJsonObject.put("address", address);
    userJsonObject.put("nric", invalidNric);
    userJsonObject.put("phone", invalidPhone);
    userJsonObject.put("active", active);
    return new HttpEntity<String>(userJsonObject.toString(), headers);
  }

  @Test
  public void updateInvalidCustomerInfo_Manager_Failure() throws Exception {
    int id = createBasicUser();
    createManager();
    URI uri = new URI(baseUrl + port + "/customers/" + id);

    ResponseEntity<String> response = restTemplate.withBasicAuth("manager", "goodpassword")
        .exchange(uri, HttpMethod.PUT, invalidUpdateCustomerRequest(), String.class);

    Map<String, Object> map = convertResponseToMap(response);

    assertEquals(400, response.getStatusCode().value());
  }

  @Test
  public void updateCustomerInfo_Customer_Success() throws Exception {
    int id = createBasicUser();
    createManager();
    URI uri = new URI(baseUrl + port + "/customers/" + id);

    ResponseEntity<String> response = restTemplate.withBasicAuth(username, password)
        .exchange(uri, HttpMethod.PUT, validUpdateCustomerRequest(), String.class);

    Map<String, Object> map = convertResponseToMap(response);

    assertEquals(200, response.getStatusCode().value());
    assertEquals(address, map.get("address"));
//    assertEquals(encoder.encode(password), map.get("password"));
    assertNotEquals(validNric, map.get("nric"));
    assertEquals(validPhone, map.get("phone"));
    assertNotEquals(active, map.get("active"));
  }

  @Test
  public void updateCustomerInfo_AnotherCustomer_Failure() throws Exception {
    int id = createBasicUser();
    createManager();
    URI uri = new URI(baseUrl + port + "/customers/" + id);

    users.save(new User("another customer", encoder.encode("password"), "ROLE_USER"));
    ResponseEntity<String> response = restTemplate.withBasicAuth("another customer", "password")
        .exchange(uri, HttpMethod.PUT, validUpdateCustomerRequest(), String.class);

    Map<String, Object> map = convertResponseToMap(response);

    assertEquals(403, response.getStatusCode().value());
  }

  @Test
  public void viewCustomerInfo_Analyst_Failure() throws Exception {
    int id = createBasicUser();
    createAnalyst();
    URI uri = new URI(baseUrl + port + "/customers/" + id);

    ResponseEntity<String> response = restTemplate.withBasicAuth("analyst_1", "01_analyst_01")
        .getForEntity(uri, String.class);

    Map<String, Object> map = convertResponseToMap(response);

    assertEquals(403, response.getStatusCode().value());
  }

  @Test
  public void updateCustomerInfo_Analyst_Failure() throws Exception {
    int id = createBasicUser();
    createAnalyst();
    URI uri = new URI(baseUrl + port + "/customers/" + id);

    ResponseEntity<String> response = restTemplate.withBasicAuth("analyst_1", "01_analyst_01")
        .exchange(uri, HttpMethod.PUT, validUpdateCustomerRequest(), String.class);

    Map<String, Object> map = convertResponseToMap(response);

    assertEquals(403, response.getStatusCode().value());
  }

}