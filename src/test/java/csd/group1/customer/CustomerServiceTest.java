package csd.group1.customer;

import csd.group1.portfolio.Portfolio;
import csd.group1.portfolio.PortfolioRepository;
import csd.group1.user.User;
import csd.group1.user.UserRepository;
import csd.group1.user.UsersServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {
  @Mock
  private UserRepository users;

  @Mock
  private PortfolioRepository portfolios;

  @InjectMocks
  private UsersServiceImpl usersService;

  @Test
  void addUser_ReturnUser() {
    User user = new User("Charlie", "goodpassword", "ROLE_USER");
    when(portfolios.save(any(Portfolio.class))).thenReturn(null);
    when(users.save(any(User.class))).thenReturn(user);

    User savedUser = usersService.addUser(user);

    assertNotNull(savedUser);
    verify(users).save(user);
  }
}
