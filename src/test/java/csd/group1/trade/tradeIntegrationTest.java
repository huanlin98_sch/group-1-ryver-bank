package csd.group1.trade;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.net.URI;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import csd.group1.account.*;
import csd.group1.stock.*;
import csd.group1.user.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class tradeIntegrationTest {  @LocalServerPort
  private int port;
  private final String baseUrl = "http://localhost:";

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private AccountRepository accounts;
    @Autowired
    private UserRepository users;
    @Autowired
    private UsersService userService;
    @Autowired
    private StockRepository stocks;
    @Autowired
    private TradeRepository trades;
    @Autowired
    private TradeService tradeService;
    @Autowired
    private BCryptPasswordEncoder encoder;
    
    private final int marketMakerID = 0;
    private final int marketMakerAccID = 0;
    private final int marketMakerQty = 20000;
    
    
    @AfterEach
    void tearDown() {
        accounts.deleteAll();
        users.deleteAll();
        trades.deleteAll();
        stocks.deleteAll();
    }

    @Test
    public void testBuy_MarketOrder_Success() throws Exception {

        URI uri = new URI(baseUrl + port + "/trades");
        stocks.save(new Stock("A17U", 3.13, 20000, 3.12, 20000, 3.27));

        User testCustomer = userService.addUser(new User("user1", encoder.encode("goodpassword"), "ROLE_USER"));
        int customerID = testCustomer.getId();
        int accountID = accounts.save(new Account(customerID, 10000.0, 10000.0)).getId();
        
        // create mock market user, account and trades
        trades.save(new Trade("A17U", "sell", marketMakerQty, marketMakerID, marketMakerAccID, 0.0, 3.27, "open"));
        //trades.save(new Trade("A17U", "buy", 20000, 0, 0, 3.13, 0.0, "open"));

        Trade trade = new Trade("A17U", "buy", 1000, customerID, accountID, 0.0, 0.0, "open");

        ResponseEntity<Trade> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri,
                trade, Trade.class);

        assertEquals(201, result.getStatusCode().value());
        assertEquals("filled", result.getBody().getStatus());
        assertEquals(trade.getQuantity(), result.getBody().getFilledQuantity());
    }

    @Test
    public void testSell_MarketOrder_Success() throws Exception {

        URI uri = new URI(baseUrl + port + "/trades");
        stocks.save(new Stock("A17U", 3.13, marketMakerQty, 3.12, 20000, 3.27));

        User testCustomer = userService.addUser(new User("user1", encoder.encode("goodpassword"), "ROLE_USER"));
        int customerID = testCustomer.getId();
        int accountID = accounts.save(new Account(customerID, 10000.0, 10000.0)).getId();
        tradeService.addToBuyerPortfolio(customerID, "A17U", 1000, 3.27);

        // create mock market user, account and trades
        trades.save(new Trade("A17U", "buy", 20000, 0, 0, 3.12, 0.0, "open"));

        // create trade to sell  
        Trade trade = new Trade("A17U", "sell", 1000, customerID, accountID, 0.0, 0.0, "open");

        ResponseEntity<Trade> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri,
                trade, Trade.class);

        assertEquals(201, result.getStatusCode().value());
        assertEquals("filled", result.getBody().getStatus());
        assertEquals(trade.getQuantity(), result.getBody().getFilledQuantity());
    }

    @Test
    public void testBuy_LimitOrder_Success() throws Exception {

        URI uri = new URI(baseUrl + port + "/trades");
        stocks.save(new Stock("A17U", 3.13, marketMakerQty, 3.12, 20000, 3.27));

        User testCustomer = userService.addUser(new User("user1", encoder.encode("goodpassword"), "ROLE_USER"));
        int customerID = testCustomer.getId();
        int accountID = accounts.save(new Account(customerID, 10000.0, 10000.0)).getId();
        tradeService.addToBuyerPortfolio(customerID, "A17U", 1000, 3.27);

        // create mock market user, account and trades
        trades.save(new Trade("A17U", "buy", 20000, 0, 0, 3.26, 0.0, "open"));

        // create trade to sell  
        Trade trade = new Trade("A17U", "sell", 1000, customerID, accountID, 0.0, 0.0, "open");

        ResponseEntity<Trade> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri,
                trade, Trade.class);

        assertEquals(201, result.getStatusCode().value());
        assertEquals("open", result.getBody().getStatus());
        assertEquals(0, result.getBody().getFilledQuantity());

    }

    @Test
    public void testSell_LimitOrder_Success() throws Exception {

        URI uri = new URI(baseUrl + port + "/trades");
        stocks.save(new Stock("A17U", 3.13, marketMakerQty, 3.12, 20000, 3.27));

        User testCustomer = userService.addUser(new User("user1", encoder.encode("goodpassword"), "ROLE_USER"));
        int customerID = testCustomer.getId();
        int accountID = accounts.save(new Account(customerID, 10000.0, 10000.0)).getId();
        tradeService.addToBuyerPortfolio(customerID, "A17U", 1000, 3.27);

        // create mock market user, account and trades
        trades.save(new Trade("A17U", "buy", 20000, 0, 0, 3.12, 0.0, "open"));

        // create trade to sell  
        Trade trade = new Trade("A17U", "sell", 1000, customerID, accountID, 0.0, 3.13, "open");

        ResponseEntity<Trade> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri,
                trade, Trade.class);

        assertEquals(201, result.getStatusCode().value());
        assertEquals("open", result.getBody().getStatus());
        assertEquals(0, result.getBody().getFilledQuantity());

    }
}
