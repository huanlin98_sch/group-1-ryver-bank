package csd.group1.portfolio;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PortfolioServiceTest {
    @Mock
    private PortfolioRepository portfolios;

    @InjectMocks
    private PortfolioServiceImpl portfolioService;

    @Test
    void addPortfolio_ReturnPortfolio() {
        Portfolio portfolio = new Portfolio(0);
        when(portfolios.save(any(Portfolio.class))).thenReturn(portfolio);

        Portfolio savedPortfolio = portfolioService.addPortfolio(portfolio);

        assertNotNull(savedPortfolio);
        verify(portfolios).save(portfolio);
    }
}
