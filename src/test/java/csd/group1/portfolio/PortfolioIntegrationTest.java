package csd.group1.portfolio;

import csd.group1.user.*;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.net.URI;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PortfolioIntegrationTest {
    @LocalServerPort
    private int port;
    private final String baseUrl = "http://localhost:";

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private UserRepository users;
    @Autowired
    private UsersServiceImpl userService;
    @Autowired
    private PortfolioRepository portfolios;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @AfterEach
    void tearDown() {
        // clear the database after each test
        users.deleteAll();
        portfolios.deleteAll();
    }

    @Test
    public void getPortfolio_User_Success() throws Exception {
        URI uri = new URI(baseUrl + port + "/portfolio");

        userService.addUser(new User("user_01", encoder.encode("goodpassword"), "ROLE_USER"));

        ResponseEntity<Portfolio> result = restTemplate.withBasicAuth("user_01", "goodpassword").getForEntity(uri,
                Portfolio.class);

        assertEquals(200, result.getStatusCode().value());
    }

    @Test
    public void getPortfolio_Analyst_NotSuccessful() throws Exception {
        URI uri = new URI(baseUrl + port + "/portfolio");

        userService.addUser(new User("analyst_01", encoder.encode("goodpassword"), "ROLE_ANALYST"));

        ResponseEntity<Portfolio> result = restTemplate.withBasicAuth("analyst_01", "goodpassword").getForEntity(uri,
                Portfolio.class);

        assertEquals(404, result.getStatusCode().value());
    }

    @Test
    public void getPortfolio_Manager_NotSuccessful() throws Exception {
        URI uri = new URI(baseUrl + port + "/portfolio");

        userService.addUser(new User("manager_01", encoder.encode("goodpassword"), "ROLE_MANAGER"));

        ResponseEntity<Portfolio> result = restTemplate.withBasicAuth("manager_01", "goodpassword").getForEntity(uri,
                Portfolio.class);

        assertEquals(404, result.getStatusCode().value());
    }

    private HttpEntity<String> makeInsights () {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject userJsonObject = new JSONObject();
        userJsonObject.put("analysis", "hello");
        return new HttpEntity<String>(userJsonObject.toString(), headers);
    }

    @Test
    public void updatePortfolio_Manager_Success() throws Exception {

        // create original portfolio
        Portfolio portfolio = new Portfolio(1, new ArrayList<>(), 0.0, 0.0, null);
        int portfolioId = portfolios.save(portfolio).getId();

        URI uri = new URI(baseUrl + port + "/portfolio/" + portfolioId);

        // edited portfolio's info (with new insight)
        List<Insight> insights = new ArrayList<>();
        Insight newInsight = new Insight();
        newInsight.setAnalysis("hello");
        insights.add(newInsight);
        Portfolio newPortfolioInfo = new Portfolio(portfolioId, new ArrayList<>(), 0.0, 0.0, insights);

        // create manager
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));

        ResponseEntity<Portfolio> result = restTemplate.withBasicAuth("manager", "goodpassword").exchange(uri,
                HttpMethod.PUT, makeInsights(), Portfolio.class);

        assertEquals(200, result.getStatusCode().value());
        assertEquals(newPortfolioInfo.getAssets(), result.getBody().getAssets());
        assertEquals(newPortfolioInfo.getUnrealizedGainLoss(), result.getBody().getUnrealizedGainLoss());
        assertEquals(newPortfolioInfo.getTotalGainLoss(), result.getBody().getTotalGainLoss());
        assertEquals(newPortfolioInfo.getInsights().get(0).getAnalysis(), result.getBody().getInsights().get(0).getAnalysis());
    }

    @Test
    public void updatePortfolio_Analyst_Success() throws Exception {

        // create original portfolio
        Portfolio portfolio = new Portfolio(1, new ArrayList<>(), 0.0, 0.0, null);
        int portfolioId = portfolios.save(portfolio).getId();

        URI uri = new URI(baseUrl + port + "/portfolio/" + portfolioId);

        // edited portfolio's info (with new insight)
        List<Insight> insights = new ArrayList<>();
        Insight newInsight = new Insight();
        newInsight.setAnalysis("hello");
        insights.add(newInsight);
        Portfolio newPortfolioInfo = new Portfolio(portfolioId, new ArrayList<>(), 0.0, 0.0, insights);

        // create analyst
        users.save(new User("Analyst", encoder.encode("goodpassword"), "ROLE_ANALYST"));

        ResponseEntity<Portfolio> result = restTemplate.withBasicAuth("Analyst", "goodpassword").exchange(uri,
            HttpMethod.PUT, makeInsights(), Portfolio.class);

        assertEquals(200, result.getStatusCode().value());
        assertEquals(newPortfolioInfo.getAssets(), result.getBody().getAssets());
        assertEquals(newPortfolioInfo.getUnrealizedGainLoss(), result.getBody().getUnrealizedGainLoss());
        assertEquals(newPortfolioInfo.getTotalGainLoss(), result.getBody().getTotalGainLoss());
        assertEquals(newPortfolioInfo.getInsights().get(0).getAnalysis(), result.getBody().getInsights().get(0).getAnalysis());
    }

    @Test
    public void updatePortfolio_Customer_Failure() throws Exception {
        // create original portfolio
        Portfolio portfolio = new Portfolio(1, new ArrayList<>(), 0.0, 0.0, null);
        int portfolioId = portfolios.save(portfolio).getId();

        URI uri = new URI(baseUrl + port + "/portfolio/" + portfolioId);

        // create user
        users.save(new User("User69", encoder.encode("goodpassword"), "ROLE_USER"));

        ResponseEntity<Portfolio> result = restTemplate.withBasicAuth("User69", "goodpassword").exchange(uri,
            HttpMethod.PUT, makeInsights(), Portfolio.class);

        assertEquals(403, result.getStatusCode().value());
    }
}
