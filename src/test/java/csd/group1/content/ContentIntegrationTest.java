package csd.group1.content;

import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import csd.group1.user.User;
import csd.group1.user.UserRepository;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ContentIntegrationTest {
    @LocalServerPort
    private int port;
    private final String baseUrl = "http://localhost:";

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private ContentRepository contents;
    @Autowired
    private UserRepository users;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @AfterEach
    void tearDown() {
        // clear the database after each test
        contents.deleteAll();
        users.deleteAll();
    }

    @Test
    public void getContents_Manager_Success() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents");
        contents.save(new Content("title", "summary", "content", "www.link.com"));
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));

        ResponseEntity<Content[]> result = restTemplate.withBasicAuth("manager", "goodpassword").getForEntity(uri,
                Content[].class);
        Content[] content = result.getBody();

        assertEquals(200, result.getStatusCode().value());
        assertEquals(1, content.length);
    }

    @Test
    public void getContents_Analyst_Success() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents");
        contents.save(new Content("title", "summary", "content", "www.link.com"));
        users.save(new User("analyst", encoder.encode("goodpassword"), "ROLE_ANALYST"));

        ResponseEntity<Content[]> result = restTemplate.withBasicAuth("analyst", "goodpassword").getForEntity(uri,
                Content[].class);
        Content[] content = result.getBody();

        assertEquals(200, result.getStatusCode().value());
        assertEquals(1, content.length);
    }

    @Test
    public void getContents_UserZeroApproved_Success() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents");
        contents.save(new Content("title", "summary", "content", "www.link.com"));
        users.save(new User("basic_user", encoder.encode("goodpassword"), "ROLE_USER"));

        ResponseEntity<Content[]> result = restTemplate.withBasicAuth("basic_user", "goodpassword").getForEntity(uri,
                Content[].class);
        Content[] content = result.getBody();

        assertEquals(200, result.getStatusCode().value());
        assertEquals(0, content.length);
    }

    @Test
    public void getContents_UserOneApproved_Success() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents");
        Content content = new Content("title", "summary", "content", "www.link.com");
        content.setApproved(true);
        contents.save(content);
        users.save(new User("basic_user", encoder.encode("goodpassword"), "ROLE_USER"));

        ResponseEntity<Content[]> result = restTemplate.withBasicAuth("basic_user", "goodpassword").getForEntity(uri,
                Content[].class);
        Content[] resultContents = result.getBody();

        assertEquals(200, result.getStatusCode().value());
        assertEquals(1, resultContents.length);
    }

    @Test
    public void getContent_Manager_Success() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));

        ResponseEntity<Content> result = restTemplate.withBasicAuth("manager", "goodpassword").getForEntity(uri,
                Content.class);

        assertEquals(200, result.getStatusCode().value());
        assertEquals(content.getTitle(), result.getBody().getTitle());
        assertEquals(content.getSummary(), result.getBody().getSummary());
        assertEquals(content.getContent(), result.getBody().getContent());
        assertEquals(content.getLink(), result.getBody().getLink());
    }

    @Test
    public void getContent_Analyst_Success() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);
        users.save(new User("analyst", encoder.encode("goodpassword"), "ROLE_ANALYST"));

        ResponseEntity<Content> result = restTemplate.withBasicAuth("analyst", "goodpassword").getForEntity(uri,
                Content.class);

        assertEquals(200, result.getStatusCode().value());
        assertEquals(content.getTitle(), result.getBody().getTitle());
        assertEquals(content.getSummary(), result.getBody().getSummary());
        assertEquals(content.getContent(), result.getBody().getContent());
        assertEquals(content.getLink(), result.getBody().getLink());
    }

    @Test
    public void getContent_InvalidContentId_Failure() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents/1");
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));

        ResponseEntity<Content> result = restTemplate.withBasicAuth("manager", "goodpassword").getForEntity(uri,
                Content.class);

        assertEquals(404, result.getStatusCode().value());
    }

    @Test
    public void getContent_UserUnapprovedContent_Failure() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);
        users.save(new User("basic_user", encoder.encode("goodpassword"), "ROLE_USER"));

        ResponseEntity<Content> result = restTemplate.withBasicAuth("basic_user", "goodpassword").getForEntity(uri,
                Content.class);

        assertEquals(404, result.getStatusCode().value());
    }

    @Test
    public void getContent_UserApprovedContent_Success() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        content.setApproved(true);
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);
        users.save(new User("basic_user", encoder.encode("goodpassword"), "ROLE_USER"));

        ResponseEntity<Content> result = restTemplate.withBasicAuth("basic_user", "goodpassword").getForEntity(uri,
                Content.class);

        assertEquals(200, result.getStatusCode().value());
        assertEquals(content.getTitle(), result.getBody().getTitle());
        assertEquals(content.getSummary(), result.getBody().getSummary());
        assertEquals(content.getContent(), result.getBody().getContent());
        assertEquals(content.getLink(), result.getBody().getLink());
    }

    @Test
    public void addContent_Manager_Success() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents");
        Content content = new Content("title", "summary", "content", "www.link.com");
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));

        ResponseEntity<Content> result = restTemplate.withBasicAuth("manager", "goodpassword").postForEntity(uri,
                content, Content.class);

        assertEquals(201, result.getStatusCode().value());
        assertEquals(content.getTitle(), result.getBody().getTitle());
        assertEquals(content.getSummary(), result.getBody().getSummary());
        assertEquals(content.getContent(), result.getBody().getContent());
        assertEquals(content.getLink(), result.getBody().getLink());
    }

    @Test
    public void addContent_Analyst_Success() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents");
        Content content = new Content("title", "summary", "content", "www.link.com");
        users.save(new User("analyst", encoder.encode("goodpassword"), "ROLE_ANALYST"));

        ResponseEntity<Content> result = restTemplate.withBasicAuth("analyst", "goodpassword").postForEntity(uri,
                content, Content.class);

        assertEquals(201, result.getStatusCode().value());
        assertEquals(content.getTitle(), result.getBody().getTitle());
        assertEquals(content.getSummary(), result.getBody().getSummary());
        assertEquals(content.getContent(), result.getBody().getContent());
        assertEquals(content.getLink(), result.getBody().getLink());
    }

    @Test
    public void addContent_User_Failure() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents");
        Content content = new Content("title", "summary", "content", "www.link.com");
        users.save(new User("basic_user", encoder.encode("goodpassword"), "ROLE_USER"));

        ResponseEntity<Content> result = restTemplate.withBasicAuth("basic_user", "goodpassword").postForEntity(uri,
                content, Content.class);

        assertEquals(403, result.getStatusCode().value());
    }

    @Test
    public void updateContent_ManagerValidBookId_Success() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        Content newContentInfo = new Content("new title", "new summary", "new content", "www.newlink.com");
        newContentInfo.setApproved(true);
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);

        ResponseEntity<Content> result = restTemplate.withBasicAuth("manager", "goodpassword").exchange(uri,
                HttpMethod.PUT, new HttpEntity<>(newContentInfo), Content.class);

        assertEquals(200, result.getStatusCode().value());
        assertEquals(newContentInfo.getTitle(), result.getBody().getTitle());
        assertEquals(newContentInfo.getSummary(), result.getBody().getSummary());
        assertEquals(newContentInfo.getContent(), result.getBody().getContent());
        assertEquals(newContentInfo.getLink(), result.getBody().getLink());
        assertEquals(newContentInfo.isApproved(), result.getBody().isApproved());
    }

    @Test
    public void updateContent_InvalidBookId_Failure() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents/1");
        Content newContentInfo = new Content("new title", "new summary", "new content", "www.newlink.com");
        newContentInfo.setApproved(true);
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));

        ResponseEntity<Content> result = restTemplate.withBasicAuth("manager", "goodpassword").exchange(uri,
                HttpMethod.PUT, new HttpEntity<>(newContentInfo), Content.class);

        assertEquals(404, result.getStatusCode().value());
    }

    @Test
    public void updateContent_AnalystValidBookId_Success() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        Content newContentInfo = new Content("new title", "new summary", "new content", "www.newlink.com");
        newContentInfo.setApproved(true);
        users.save(new User("analyst", encoder.encode("goodpassword"), "ROLE_ANALYST"));
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);

        ResponseEntity<Content> result = restTemplate.withBasicAuth("analyst", "goodpassword").exchange(uri,
                HttpMethod.PUT, new HttpEntity<>(newContentInfo), Content.class);

        assertEquals(200, result.getStatusCode().value());
        assertEquals(newContentInfo.getTitle(), result.getBody().getTitle());
        assertEquals(newContentInfo.getSummary(), result.getBody().getSummary());
        assertEquals(newContentInfo.getContent(), result.getBody().getContent());
        assertEquals(newContentInfo.getLink(), result.getBody().getLink());
        assertEquals(false, result.getBody().isApproved());
    }

    @Test
    public void updateContent_User_Failure() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        Content newContentInfo = new Content("new title", "new summary", "new content", "www.newlink.com");
        users.save(new User("basic_user", encoder.encode("goodpassword"), "ROLE_USER"));
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);

        ResponseEntity<Content> result = restTemplate.withBasicAuth("basic_user", "goodpassword").exchange(uri,
                HttpMethod.PUT, new HttpEntity<>(newContentInfo), Content.class);

        assertEquals(403, result.getStatusCode().value());
    }

    @Test
    public void deleteBook_ManagerValidBookId_Success() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));

        ResponseEntity<Void> result = restTemplate.withBasicAuth("manager", "goodpassword").exchange(uri,
                HttpMethod.DELETE, null, Void.class);

        assertEquals(200, result.getStatusCode().value());
        Optional<Content> emptyValue = Optional.empty();
        assertEquals(emptyValue, contents.findById(id));
    }

    @Test
    public void deleteBook_AnalystValidBookId_Success() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);
        users.save(new User("analyst", encoder.encode("goodpassword"), "ROLE_ANALYST"));

        ResponseEntity<Void> result = restTemplate.withBasicAuth("analyst", "goodpassword").exchange(uri,
                HttpMethod.DELETE, null, Void.class);

        assertEquals(200, result.getStatusCode().value());
        Optional<Content> emptyValue = Optional.empty();
        assertEquals(emptyValue, contents.findById(id));
    }

    @Test
    public void deleteBook_InvalidBookId_Failure() throws Exception {
        URI uri = new URI(baseUrl + port + "/contents/1");
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));

        ResponseEntity<Void> result = restTemplate.withBasicAuth("manager", "goodpassword").exchange(uri,
                HttpMethod.DELETE, null, Void.class);

        assertEquals(404, result.getStatusCode().value());
    }

    @Test
    public void deleteBook_UserValidBookId_Failure() throws Exception {
        Content content = new Content("title", "summary", "content", "www.link.com");
        int id = contents.save(content).getId();
        URI uri = new URI(baseUrl + port + "/contents/" + id);
        users.save(new User("basic_user", encoder.encode("goodpassword"), "ROLE_USER"));

        ResponseEntity<Void> result = restTemplate.withBasicAuth("basic_user", "goodpassword").exchange(uri,
                HttpMethod.DELETE, null, Void.class);

        assertEquals(403, result.getStatusCode().value());
    }
}