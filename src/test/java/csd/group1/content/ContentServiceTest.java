package csd.group1.content;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ContentServiceTest {
    @Mock
    private ContentRepository contents;

    @InjectMocks
    private ContentServiceImpl contentService;

    @Test
    void addContent_NewContent_ReturnSavedContent() {
        Content content = new Content("title", "summary", "content", "link");

        when(contents.save(any(Content.class))).thenReturn(content);

        Content savedContent = contentService.addContent(content);

        assertNotNull(savedContent);
        verify(contents).save(content);
    }

    @Test
    void updateContent_NotFound_ReturnNull() {
        Content content = new Content("title", "summary", "content", "link");
        int contentId = 100;
        when(contents.findById(contentId)).thenReturn(Optional.empty());

        Content updatedContent = contentService.updateContent(contentId, content);

        assertNull(updatedContent);
        verify(contents).findById(contentId);
    }

    @Test
    void analystContentUpdate_NotFound_ReturnNull() {
        Content content = new Content("title", "summary", "content", "link");
        int contentId = 100;
        when(contents.findById(contentId)).thenReturn(Optional.empty());

        Content updatedContent = contentService.analystContentUpdate(contentId, content);

        assertNull(updatedContent);
        verify(contents).findById(contentId);
    }
}
