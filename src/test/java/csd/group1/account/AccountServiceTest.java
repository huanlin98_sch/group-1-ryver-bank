package csd.group1.account;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
    
    @Mock
    private AccountRepository accounts;

    @InjectMocks
    private AccountServiceImpl accountServiceImpl;

    //Adding of account
    @Test
    void addAccount_NewAccount_ReturnSavedAccount() {
        Account account = new Account(1, 1000.0, 1000.0);

        //mock save
        when(accounts.save(any(Account.class))).thenReturn(account);

        //act
        Account savedAccount = accountServiceImpl.addAccount(account);

        //assert
        assertNotNull(savedAccount);
        verify(accounts).save(account);
    }
}
