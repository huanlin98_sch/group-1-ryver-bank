package csd.group1.account;

import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import csd.group1.user.*;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AccountIntegrationTest {
    @LocalServerPort
    private int port;
    private final String baseUrl = "http://localhost:";

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private AccountRepository accounts;
    @Autowired
    private UserRepository users;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @AfterEach
    void tearDown() {
        accounts.deleteAll();
        users.deleteAll();
    }

    //getAllOwnAccounts (user get all of own accounts -- success)
    @Test
    public void getAllOwnAccounts_User_Success() throws Exception {
        URI uri = new URI(baseUrl + port + "/accounts");

        //create user1
        int customerID = users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER")).getId();

        //create accounts
        accounts.save(new Account(customerID, 1000.0, 1000.0));
        accounts.save(new Account(customerID, 2000.0, 2000.0));

        ResponseEntity<Account[]> result = restTemplate.withBasicAuth("user1", "goodpassword").getForEntity(uri, Account[].class);
        Account[] account = result.getBody();

        assertEquals(200, result.getStatusCode().value());
        assertEquals(2, account.length);
    }

    //getAccount (user get one of his accounts -- success)
    @Test
    public void getAccount_ValidUser_Success() throws Exception {
        //create user1
        int customerID = users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER")).getId();

        //create account (id = 1)
        Account account = new Account(customerID, 1000.0, 1000.0);
        Integer id = accounts.save(account).getId();

        URI uri = new URI(baseUrl + port + "/accounts/" + id);

        ResponseEntity<Account> result = restTemplate.withBasicAuth("user1", "goodpassword").getForEntity(uri, Account.class);

        assertEquals(200, result.getStatusCode().value());
        assertEquals(account.getId(), result.getBody().getId());
        assertEquals(account.getAvailableBalance(), result.getBody().getAvailableBalance());
        assertEquals(account.getBalance(), result.getBody().getBalance());
    }

    @Test
     //addAccount (manager add account for user -- success)
     public void addAccount_Manager_ValidUser_Success() throws Exception {
        //create manager
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));
        //create user2
        int customerID = users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER")).getId();
        
        //create account object
        Account account = new Account(customerID, 1000.0, 1000.0);

        URI uri = new URI(baseUrl + port + "/accounts");

        ResponseEntity<Account> result = restTemplate.withBasicAuth("manager", "goodpassword").postForEntity(uri, account, Account.class);

        assertEquals(201, result.getStatusCode().value());
        assertEquals(account.getAvailableBalance(), result.getBody().getAvailableBalance());
        assertEquals(account.getBalance(), result.getBody().getBalance());
    }

    @Test
    //getAccount (user get someone else's account -- failure)
    public void getAccount_InvalidUser_Failure() throws Exception {
        //create user1
        users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER"));
        //create user2
        users.save(new User("user2", encoder.encode("goodpassword"), "ROLE_USER"));

        //create account (id = 2)
        Account account = new Account(2, 1000.0, 1000.0);
        Integer id = accounts.save(account).getId();

        URI uri = new URI(baseUrl + port + "/accounts/" + id);

        ResponseEntity<Account> result = restTemplate.withBasicAuth("user1", "goodpassword").getForEntity(uri, Account.class);

        //AccountForbiddenException (403)
        assertEquals(403, result.getStatusCode().value());
    }

    @Test
    //getAccount (user tries to get inexistent account -- failure)
    public void getAccount_ValidUser_InexistentAccount_Failure() throws Exception {
        //create user1
        users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER"));

        URI uri = new URI(baseUrl + port + "/accounts/" + 1);

        ResponseEntity<Account> result = restTemplate.withBasicAuth("user1", "goodpassword").getForEntity(uri, Account.class);

        //AccountNotFoundException (404)
        assertEquals(404, result.getStatusCode().value());
    }

    @Test
    //addAccount (manager add account for inexistent user -- failure)
    public void addAccount_Manager_InvalidUser_Failure() throws Exception {
        //create manager
        users.save(new User("manager", encoder.encode("goodpassword"), "ROLE_MANAGER"));     

        //create account object
        Account account = new Account(2, 1000.0, 1000.0);

        URI uri = new URI(baseUrl + port + "/accounts");

        ResponseEntity<Account> result = restTemplate.withBasicAuth("manager", "goodpassword").postForEntity(uri, account, Account.class);

        //UserNotFoundException (404)
        assertEquals(400, result.getStatusCode().value());
    }

    @Test
    //addAccount (user tries to add account -- failure)
    public void addAccount_User_Failure() throws Exception {
        //create manager
        users.save(new User("user1", encoder.encode("goodpassword"), "ROLE_USER"));     

        //create account object
        Account account = new Account(1, 1000.0, 1000.0);

        URI uri = new URI(baseUrl + port + "/accounts");

        ResponseEntity<Account> result = restTemplate.withBasicAuth("user1", "goodpassword").postForEntity(uri, account, Account.class);

        //Forbidden (404)
        assertEquals(403, result.getStatusCode().value());
    }

    @Test
    //addAccount (analyst tries to add account -- failure)
    public void addAccount_Analyst_Failure() throws Exception {
        //create manager
        users.save(new User("analyst", encoder.encode("goodpassword"), "ROLE_ANALYST"));     

        //create account object
        Account account = new Account(1, 1000.0, 1000.0);

        URI uri = new URI(baseUrl + port + "/accounts");

        ResponseEntity<Account> result = restTemplate.withBasicAuth("analyst", "goodpassword").postForEntity(uri, account, Account.class);

        //Forbidden (404)
        assertEquals(403, result.getStatusCode().value());
    }
}
